Virtual env

pip install virtualenv
virtualenv --python=/usr/bin/python3 env_name

source env_name/bin/activate

source venv/bin/activate

deactivate

python manage.py runserver 0.0.0.0:8080
./manage.py runserver 0.0.0.0:8080

./manage.py check
./manage.py cms check

##djangocms

pip install djangocms-installer
djangocms -p . project
pip3 install django-imagekit


### django

# django-admin startproject cms

python manage.py makemigrations news
python manage.py migrate
python manage.py collectstatic


python manage.py runserver 0.0.0.0:8080
python manage.py runserver 127.0.0.1:8080

python manage.py createsuperuser

python manage.py startapp news

djnago-admin makemessages -l fi

djnago-admin compilemessages -l fi

## before commiting
rm -R */migrations/
rm -R */__pycache__/
rm -R newssite/media/featured/
rm db.sqlite3

## migrate apps
python manage.py makemigrations news
python manage.py makemigrations AdMgmt
python manage.py makemigrations usermgt
python manage.py makemigrations survey
python manage.py makemigrations subscription
python manage.py makemigrations restapi
python manage.py makemigrations restapimobile
python manage.py makemigrations comments
python manage.py makemigrations polls
python manage.py makemigrations theme
python manage.py makemigrations user_auth
python manage.py makemigrations seo
python manage.py makemigrations analytics
python manage.py makemigrations sharing
python manage.py makemigrations ratings



## dump data

python manage.py dumpdata theme.PlaceHolder --indent 2 > placeholder.json
python manage.py dumpdata theme.SiteSettings theme.PlaceHolder --indent 2 > themedata.json
python manage.py dumpdata news.Category --indent 2 > categories.json
python manage.py dumpdata news.SiteSlug --indent 2 > slugs.json
python manage.py dumpdata theme.FooterSettings --indent 2 > footer.json
python manage.py dumpdata theme.SidebarSettings --indent 2 > sidebar.json
python manage.py dumpdata usermgt.UserRoles --indent 2 > userroles.json
python manage.py dumpdata usermgt.UserCapability --indent 2 > usercapability.json
python manage.py dumpdata usermgt.RoleCapability --indent 2 > rolecapability.json

python manage.py loaddata placeholder.json
python manage.py loaddata themedata.json
python manage.py loaddata slugs.json
python manage.py loaddata categories.json
python manage.py loaddata footer.json
python manage.py loaddata sidebar.json
python manage.py loaddata userroles.json
python manage.py loaddata usercapability.json
python manage.py loaddata rolecapability.json


## reset database
rm -R newssite/migrations
rm db.sqlite3

# youtube video

https://www.youtube.com/watch?v=o6yYygu-vvk&index=21&list=PLGzru6ACxEAIy3lucCGrzqxzGhjPeZey_


### admin theme
python manage.py loaddata admin_interface_theme_uswds.json

## git merge cmds

git checkout master
git merge dev
git add '{your updated files}'
git commit
git push

## git rebase https://git-scm.com/book/en/v2/Git-Branching-Rebasing

# get latest commit hash
git rev-parse HEAD

#pull branch changes
git pull origin branch
git pull origin comments --strategy=ours
git pull -X theirs



#list all branches
git branch -a
