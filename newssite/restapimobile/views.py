from django.shortcuts import render

from rest_framework.views import APIView 
from .serializers import NewsSerializer, Serializer,SortSerializer,SortByPop,TagSerializer,ApiSerializer,NewsSerializerId,NewsUpdateSerializer
from rest_framework.response import Response
from django.utils.crypto import get_random_string

from news.models import News,Category,Tag
from ratings.models import Rating
from django.contrib.auth.models import User
from restapimobile.models import ApiKeyModel
from django.core.exceptions import ValidationError

from django.db import transaction
from django.db.models import F
from django.http import JsonResponse
from django.core.paginator import Paginator
# from django.http import HttpResponse

from datetime import datetime, timedelta


# Create your views here.
DEFAULT_ITEMS = 10

class RestView(APIView):
	def get(self,request,site):
		if request.method == "GET":
			obj_cat = CategoryF(request,site)
			items = int(request.GET.get('items',0))
			if items <= 0 or items >50:
				items = DEFAULT_ITEMS
			paginator = Paginator(obj_cat, items)
			page = request.GET.get('page','')
			cat_page = paginator.get_page(page)
			serializer_cat = Serializer(cat_page,many=True)
			return Response(serializer_cat.data)

class TagView(APIView):
	def get(self,request,site):
		if request.method == "GET":
			get_tag = TagF(request,site)
			items = int(request.GET.get('items',0))
			if items <= 0 or items >50:
				items = DEFAULT_ITEMS
			paginator = Paginator(get_tag, items)
			page = request.GET.get('page','')
			cat_page = paginator.get_page(page)
			serializer_tag = TagSerializer(get_tag,many=True)
			return Response(serializer_tag.data)

#category
def CategoryF(site):
	category = Category.objects.filter(site=site)
	category_list = list(category)
	# counter = category.count()
	return category_list

# tag
def TagF(request,site):
	print(site)
	tag = Tag.objects.filter(site=site)
	return tag

class SortView(APIView):

	def get(self,request):
		if request.method == "GET":
			get_obj = SortByDate(request)
			serializer = SortSerializer(get_obj,many=True)
			return Response(serializer.data)

# sort by published date
def SortByDate(request):
	obj_all = News.objects.all()
	print (News.objects.all().count())
	for o in obj_all:
		obj_status = News.objects.filter(status="published").order_by('-published')
		print(obj_status)
		return obj_status

# sort by popularity
class SortByPopularity(APIView):
	def get(self,request):
			News.objects.filter(
				news__id = share_view.objects.filter())

			serializer = SortByPop(rating_obj,many=True)
			return Response(serializer.data)

# get category_id
class GetCategoryView(APIView):
	def get(self,request):
		if request.method == "GET":
			obj_gcat = GetCategoryF(request)
			serializer = NewsSerializer(obj_gcat,many=True)
			return Response(serializer.data)

def GetCategoryF(request):
	category = request.GET.get('category','')
	catobj = Category.objects.get(id = category)
	news = News.objects.filter(category=catobj)
	print (News.objects.filter(category=catobj).count())
	return news
			
class ApiView(APIView):
	def get(self,request):
		if request.method == "GET":
			api_check = CheckApi(request)
			
			if api_check is 'unreviewed':
				context = {"error": True, "Message":'Your API application isnt reviewed yet'}
				return JsonResponse(context)
			elif  api_check is  'unsupported_api_key':
				context = {"error": True, 'Message':'Your API Key is not valid'}
				return JsonResponse(context)
			elif  api_check is  'too_many_api_calls':
				context = {"error": True, 'Message':'You are over using API Key'}
				return JsonResponse(context)
			else:
				serializer_api = ApiSerializer(api_check)
				return Response(serializer_api.data)


def ApiKeyView(request):
	if request.method == "GET":
		api_key = get_random_string(length=64)
		print(api_key)
		if request.user.is_authenticated:
			users = User.objects.get(id=request.user.id)
			print(users)
			a = ApiKeyModel()
			a.users = users
			print(a.users)
			a.api_key = api_key
			a.count_pd = 0
			a.reviewed = True
			a.save()
		else:
			return render(request, "registration/login.html")
	return render(request,"restapimobile/restapimobile.html")

def CheckApi(request):
		if request.user.is_authenticated:
			user = User.objects.get(id=request.user.id)
			api_key = request.GET.get('api_key','')

			print(user)
			obj_type = ApiKeyModel.objects.get(users=user)
			print(obj_type.api_key)
			print(api_key)
			if not obj_type.reviewed :
				return "unreviewed"
			elif not obj_type.api_key == api_key:
				return "unsupported_api_key"
			elif obj_type.count >= obj_type.count_pd and obj_type.api_type == 'st':
				return "too_many_api_calls"
			else:
				obj_type.count = obj_type.count+1
				obj_type.save()
				return obj_type

class NewsIdViews(APIView):
	def get(self,request,pid):
		if request.method == "GET":
			obj_news = NewsIdF(request,pid)
			serializer = NewsSerializerId(obj_news,many=True)
			return Response(serializer.data)

def NewsIdF(request,pid):
	news_id = News.objects.filter(id=pid)
	return news_id

class NewsUpdateViews(APIView):
	def get(self,request,pid):
		if request.method == "GET":
			obj_news = NewsUpdateF(request,pid)
			serializer = NewsUpdateSerializer(obj_news,many=True)
			return Response(serializer.data)

def NewsUpdateF(request,pid):
	news = News.objects.filter(id=pid)
	return news