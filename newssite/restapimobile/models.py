from django.db import models
from django.contrib.auth.models import User

# Create your models here.
APIKEY_OPTIONS = (
        ('su','Super'),
        ('st','Standard '),
        )

class ApiKeyModel(models.Model):
	users = models.OneToOneField(User, on_delete=models.CASCADE ,related_name ='users', verbose_name = "User",unique=True)
	api_key = models.CharField(verbose_name = "Api key",max_length=64)
	count_pd = models.IntegerField(verbose_name = "Count per day")
	count = models.IntegerField(default=0,verbose_name = "Count")
	api_type = models.CharField(max_length=3,choices=APIKEY_OPTIONS,default='st')
	last_fetched = models.DateTimeField(auto_now=True, verbose_name = "Last fetched")
	reviewed = models.BooleanField(default=False)