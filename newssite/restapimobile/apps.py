from django.apps import AppConfig


class RestapimobileConfig(AppConfig):
    name = 'restapimobile'
