from django.contrib import admin
from .models import ApiKeyModel

# Register your models here.
admin.site.register(ApiKeyModel)