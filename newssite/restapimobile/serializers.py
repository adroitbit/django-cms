from rest_framework import serializers
from news.models import Category,News,Tag
from ratings.models import Rating
from restapimobile.models import ApiKeyModel

class Serializer(serializers.ModelSerializer):
	title= serializers.CharField(max_length=50)
	site = serializers.CharField(max_length=4)
	total_cat = serializers.SerializerMethodField('CategoryF')

	def CategoryF(self,foo):
		return foo.counter == "obj_cat"

	class Meta:
		model = Category
		fields = ['title','site','total_cat']

class NewsSerializer(serializers.ModelSerializer):
	content= serializers.CharField(max_length=50)
	updated  = serializers.DateTimeField()

	# site = serializers.CharField(max_length=4)
	class Meta:
		model = Category
		fields = ['title', 'content', 'id', 'updated']

		
class SortSerializer(serializers.ModelSerializer):
	title= serializers.CharField(max_length=50)
	# site = serializers.CharField(max_length=4)
	updated  = serializers.DateTimeField()
	class Meta:
		model = News
		fields = ['title','category','updated']

class SortByPop(serializers.ModelSerializer):
	rating_s = serializers.CharField(max_length=160)
	# site = serializers.CharField(max_length=4)
	class Meta:
		model = Rating
		fields = ['news','rating_s']

class TagSerializer(serializers.ModelSerializer):
	title= serializers.CharField(max_length=50)
	site = serializers.CharField(max_length=4)
	class Meta:
		model = Tag
		fields = ['title','site','id']

class ApiSerializer(serializers.ModelSerializer):
	class Meta:
		model = ApiKeyModel
		fields = ['users','api_key','api_type','reviewed']

class NewsSerializerId(serializers.ModelSerializer):
	class Meta:
		model = News
		fields = ['id','title','category','content']

class NewsUpdateSerializer(serializers.ModelSerializer):
	class Meta:
		model = News
		fields = ['id','title','updated']		