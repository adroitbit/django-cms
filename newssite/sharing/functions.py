from django.db import models
from django_user_agents.utils import get_user_agent

from .models import Shares, ShareData
# from datetime import datetime
from django.db.models import Subquery, OuterRef
from django.db.models import Count, Sum
import datetime
from django.utils import timezone

from news.models import News, Category


def get_most_shares(cat,sevendaysago=False):
    current_date= timezone.now()
    seven = (timezone.now() - datetime.timedelta(days=7))
    if sevendaysago is False:
        slug_views_sq = Subquery(
            ShareData.objects
            .filter(slug=OuterRef('slug'))
            .values('slug')
            .annotate(shares=Sum('shares'))
            .values('shares')[:1],
            output_field=models.IntegerField()
            )
    else:
        slug_views_sq = Subquery(
            Shares.objects
            .filter(slug=OuterRef('slug'),date__range= [seven, current_date])
            .values('slug')
            .annotate(shares=Sum('shares'))
            .values('shares')[:1],
            output_field=models.IntegerField()
            )
    slug_views_sq.contains_aggregate = False
    return News.objects.filter(status='published',category=cat).annotate(shares =slug_views_sq).order_by('-shares')
    
    
    