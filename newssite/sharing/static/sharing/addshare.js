$(function(){
    "use strict";

function shareFB(shareid){
  var url =  window.location+"?share="+shareid;
  console.log(url);
  var appid = $("meta[property='fb:app_id']").attr('content');
  var post_title = $(".news-post h2.new-title").text();
  if (appid){
    console.log(appid);
    var data = {
      'method': 'share',
      'redirect_uri':window.location,
      'app_id':appid,
      'display':'popup',
      'href':url,
      'quote':post_title,
    };
    var link = 'https://www.facebook.com/dialog/share?redirect_uri='+encodeURIComponent(data['redirect_uri'])+"&href="+encodeURIComponent(data['href'])+"&app_id="+encodeURIComponent(data['app_id'])+"&display="+encodeURIComponent(data['display'])+"&quote="+encodeURIComponent(data['quote']);
    window.location.href= link;

  }

}

function share_(shareid, site){
  if (site =='facebook'){
    shareFB(shareid);
  }
  // else if (false) {
  //
  // }
}
$(".share-dropdown").on('click', function() {
        event.preventDefault();
        // var user = $('#id_user').val();
        // var news = $('#id_news').val();
        // var data = {'user':user,'news':news}
        // console.log(data)
        var origin  = $(this);
        var site = origin.attr("data-site");
        var newsid = parseInt($('#share_newsid').val());
        // var newsid = parseInt($('#id_news').val());
        var shareid= $('#shareid').val();
        var token = $('#sharebuttonForm input[name="csrfmiddlewaretoken"]').val();
        // FB.ui({
        //     display:'popup',
        //     method:'share',
        //     href:'/news-first/',
        // },function(response){});
        var data = {
          "id":newsid,
          'csrfmiddlewaretoken':token,
        };
        console.log("Shared");
        console.log(shareid);
        console.log(newsid);

        if (shareid){
          share_(shareid, site);
        }else{
          $.ajax({
                   type:"POST",
                   url:"/news_share/",
                   data:data,
                   success: function(resp){
                       if(resp.success){
                          share_(resp.share_id, site);
                          console.log(resp.share_id);
                          $('#shareid').val(resp.share_id);
                          console.log("success");
                       }else{
                           console.log("Something went wrong")
                       }
                      }
              });
        }


    });


});
