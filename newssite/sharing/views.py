from django.shortcuts import render

from news.models import SiteSlug
from news.models import News
from sharing.models import Shares, ShareClicks, ShareData
from django.contrib.auth.models import User
import json
from django.http import HttpResponse
from django.http import JsonResponse
from rest_framework.response import Response
from django.shortcuts import redirect
from django.utils.crypto import get_random_string
from django.utils import timezone

# Create your views here.
def share_news(request):
	if request.method == "POST":
		news_id = int(request.POST.get('id',""))
		user_id = request.POST.get('user','unregistered')
		print(news_id)
		try:
			# siteslug = SiteSlug.objects.get(slug=slug)
			news_obj =  News.objects.get(id=news_id)
			print(news_obj)
			share_id = get_random_string(length=32)
			o =Shares()

			if request.user.is_authenticated:
				user = User.objects.get(id=request.user.id)
				o.user=user.username
			else:
				# user = User.objects.get(id=request.user.id)
				o.user=user_id

			o.share_id= share_id
			o.slug = news_obj.slug
			o.save()
			incriment_share_count(news_obj.slug)
			context={
				"share_id":share_id,
				'success':True,
			}
			return JsonResponse(context)
		except News.DoesNotExist:
			context={
				"share_id":"",
				"error":"Does not exists"
			}
			return JsonResponse(context)


from django_user_agents.utils import get_user_agent
from analytics.models import get_ua_object

def get_shareData(slug, date=timezone.now().date()):
	try:
		sdata = ShareData.objects.get(slug=slug, date=date)
		return sdata
	except ShareData.DoesNotExist:
		return None
		
def incriment_share_count(slug, date=timezone.now().date()):
	sdata = get_shareData(slug,date)
	if not sdata is None:
		sdata.shares +=1
		sdata.save()
	else:
		obj = ShareData()
		obj.slug = slug
		obj.shares = 1
		obj.save()
		
		
def incriment_click_count(slug, date=timezone.now().date()):
	sdata = get_shareData(slug,date)
	if not sdata is None:
		sdata.click +=1
		sdata.save()
	else:
		obj = ShareData()
		obj.slug = slug
		obj.click = 1
		obj.save()
	
def monitor_shares(request):
	if request.method == "GET":
		share_id = request.GET.get('share','')
		user_agent = get_user_agent(request)
		obj = get_ua_object(user_agent)
		try:
			ref = request.META.get('HTTP_REFERER', '')
			s =ShareClicks()
			s.model = shareview
			s.user_agent = obj
			s.referrer = ref
			s.save()
			
			shareview = Shares.objects.get(share_id=share_id)
			incriment_click_count(shareview.slug)
			
			return True
		except Shares.DoesNotExist:
			return False