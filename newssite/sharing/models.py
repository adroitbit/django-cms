from django.db import models

from django.contrib.auth.models import User
from news.models import News, SiteSlug
from analytics.models import UserAgent
# Create your models here.
class Shares(models.Model):
	share_id = models.CharField(verbose_name = "Uniques Share ID", unique=True, max_length=64)
	user = models.CharField(verbose_name = "Username", max_length=64)
	# click = models.IntegerField(default=0 ,verbose_name = "Url clicks")
	# news = models.ForeignKey(News,on_delete=models.CASCADE)
	slug = models.ForeignKey(SiteSlug,on_delete=models.CASCADE)

class ShareData(models.Model):
	shares = models.IntegerField(default=0 ,verbose_name = "Url clicks")
	click = models.IntegerField(default=0 ,verbose_name = "Url clicks")
	slug = models.ForeignKey(SiteSlug,on_delete=models.CASCADE)
	date = models.DateField(auto_now=True)

class ShareClicks(models.Model):
	model = models.ForeignKey(Shares,on_delete=models.CASCADE)
	referrer = models.URLField(verbose_name = "Referrer URL",max_length=512)
	user_agent = models.ForeignKey(UserAgent,on_delete=models.CASCADE, verbose_name = "User Agent")
	datetime = models.DateTimeField(auto_now=True, verbose_name = "Clicked")
