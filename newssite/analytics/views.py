from django.shortcuts import render

from news.models import SiteSlug 
from news.models import Category
from news.models import Tag
from news.models import News
from analytics.models import AnalyticsReal,AnalyticalData
from datetime import date
from django.http import JsonResponse
# Create your views here.
def analytics_view(request):
	if request.method == "GET":
		slug = request.GET.get('slug','')
		print(slug)
		ua = request.META.get('HTTP_USER_AGENT', '')
		print(ua)
		siteslug = SiteSlug.objects.get(slug=slug)
		print(siteslug)
		slug_obj =  News.objects.filter(slug=siteslug)
		print(slug_obj)
		# elif slug_obj = Category.objects.filter(slug=siteslug)[0]:
		# 	print(slug_obj)
		# elif slug_obj = Tag.objects.filter(slug=siteslug)[0]:
		# 	print(slug_obj)
		if request.user.is_authenticated:
			o =AnalyticsReal()
			o.user_agent = ua
			o.slug = siteslug
			o.save()
		analyticspd(request)
	return render(request,"sharing/shared.html")

def analyticspd(request):
	if request.method == "GET":
		slug = request.GET.get('slug','')
		print(slug)
		siteslug = SiteSlug.objects.get(slug=slug)
		print(siteslug)
		analytics_view = AnalyticsReal.objects.filter(slug=siteslug)
		print(analytics_view)
		obj_count = AnalyticsReal.objects.all().count()
		print(obj_count)
		if obj_count>=1000:
			print("count is greater")
			if analytics_view is not None:
				for obj in analytics_view:
					analytics = AnalyticalData.objects.filter(slug=obj.slug, date=obj.datetime.date()).first()
					if analytics is not None:
						print("yes")
						analytics.views +=1
						analytics.save()
					else:
						print("no")
						n = AnalyticalData()
						n.views =1
						n.slug = siteslug
						n.date = obj.datetime.date()
						n.save()
					obj.delete()
			
		context = {
		 "slug":slug
		 }
	return JsonResponse(context) 	

# def data_added():
# 	obj_count = AnalyticsReal.objects.all().count()
# 	print(obj_count)
# 	if obj_count>=10:
# 		analytics_view = AnalyticsReal.objects.all()
# 		print(analytics_view)

# 		if analytics_view is not None:
# 			for obj in analytics_view.all():
# 				analytics = AnalyticalData.objects.filter(slug=obj.slug, date=obj.datetime.date()).first()
# 				if analytics is not None:
# 					analytics.views +=1
# 					analytics.save()
# 				else:
# 					n = AnalyticalData()
# 					n.views =1
# 					n.slug = siteslug
# 					n.date = obj.datetime.date()
# 					n.save()
# 				obj.delete()