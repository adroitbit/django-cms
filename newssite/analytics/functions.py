from django.db import models
from django_user_agents.utils import get_user_agent

from .models import get_ua_object, AnalyticsReal, AnalyticalData
# from datetime import datetime
from django.db.models import Subquery, OuterRef
from django.db.models import Count
import datetime
from django.utils import timezone

import pytz

from news.models import News, Category

def update_analytics(request, slugObj):
    user_agent = get_user_agent(request)
    obj = get_ua_object(user_agent)
    print(obj)
    realdata = AnalyticsReal.objects.create(
        user_agent= obj,
        slug = slugObj
        )
    # time = datetime.datetime.now()
    dailydata = AnalyticalData.objects.filter(slug=slugObj, date=realdata.datetime.date()).first()
    if dailydata is not None:
        dailydata.views += 1
        dailydata.save()
    else:
        n = AnalyticalData.objects.create(
            views=1,
            slug = slugObj,
            date = realdata.datetime.date()
            )
            
            
def get_most_viewed(cat,sevendaysago=False):
    current_date= timezone.now()
    seven = (timezone.now() - datetime.timedelta(days=7))
    if sevendaysago is False:
        slug_views_sq = Subquery(
            AnalyticsReal.objects
            .filter(slug=OuterRef('slug'))
            .values('slug')
            .annotate(views=Count('slug'))
            .order_by('views')
            .values('views')[:1],
            output_field=models.IntegerField()
            )
    else:
        slug_views_sq = Subquery(
            AnalyticsReal.objects
            .filter(slug=OuterRef('slug'),datetime__range= [seven, current_date])
            .values('slug')
            .annotate(views=Count('slug'))
            .values('views')[:1],
            output_field=models.IntegerField()
            )
    slug_views_sq.contains_aggregate = False
    return News.objects.filter(status='published',category=cat).annotate(views =slug_views_sq).order_by('-views')
    
    
    