from django.contrib import admin

from analytics.models import AnalyticalData,AnalyticsReal
# Register your models here.
# admin.site.register(AnalyticalData)



class AnalyticalRealDataAdmin(admin.ModelAdmin):
	date_hierarchy = 'datetime'
	list_display = ('slug','datetime','user_agent')
	actions = ['accumulate_analytics']

	def accumulate_analytics(self,request,queryset):
		for obj in queryset.all():
				analytics = AnalyticalData.objects.filter(slug=obj.slug, date=obj.datetime.date()).first()
				if analytics is not None:
					analytics.views +=1
					analytics.save()
				else:
					n = AnalyticalData()
					n.views =1
					n.slug = siteslug
					n.date = obj.datetime.date()
					n.save()
				obj.delete()
	accumulate_analytics.short_description = "Accumulate selected"
admin.site.register(AnalyticsReal, AnalyticalRealDataAdmin)

class AnalyticalDataAdmin(admin.ModelAdmin):
	date_hierarchy = 'date'
	list_display = ('slug','date','views')
	

	def accumulate_analytics(self,request,queryset):
		for q in queryset:
			print(q.slug)
			print(q.date)
	accumulate_analytics.short_description = "Accumulate selected"
admin.site.register(AnalyticalData,AnalyticalDataAdmin)