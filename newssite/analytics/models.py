from django.db import models

from news.models import SiteSlug

from django.db.models.signals import m2m_changed

class UserAgent(models.Model):
	class Meta:
		unique_together = ('is_mobile', 'is_tablet','is_touch_capable','is_pc','is_bot', 'browser_family','browser_version','os_family', 'os_version', 'device_family')
	is_mobile = models.BooleanField(verbose_name = "Is mobile", default=False)
	is_tablet = models.BooleanField(verbose_name = "Is Tablet", default=False)
	is_touch_capable = models.BooleanField(verbose_name = "Is Touch capable", default=False)
	is_pc = models.BooleanField(verbose_name = "Is PC", default=False)
	is_bot = models.BooleanField(verbose_name = "Is Bot", default=False)
	browser_family =  models.CharField(verbose_name = "Browser Family", max_length=50)
	browser_version =  models.CharField(verbose_name = "Browser version", max_length=50)
	os_family =  models.CharField(verbose_name = "OS Family", max_length=50)
	os_version =  models.CharField(verbose_name = "OS version", max_length=50)
	device_family =  models.CharField(verbose_name = "Device family", max_length=50)
	def __str__(self):
		return "Device: "+ self.device_family +" OS:"+ self.os_family +" (v"+ self.os_version + ")"


# Create your models here.
class AnalyticalData(models.Model):
	class Meta:
		unique_together = ('slug', 'date')
	views = models.PositiveIntegerField(default=0,verbose_name = "Views per day")
	slug = models.ForeignKey(SiteSlug,on_delete=models.CASCADE)
	date = models.DateField(auto_now=True)

class AnalyticsReal(models.Model):
	user_agent = models.ForeignKey(UserAgent,verbose_name = "User Agent",on_delete=models.CASCADE)
	datetime = models.DateTimeField(auto_now=True)
	slug = models.ForeignKey(SiteSlug,on_delete=models.CASCADE)

        
def get_ua_object(useragent):
	try:
		ua =  UserAgent.objects.get(
			is_mobile=useragent.is_mobile, 
			is_tablet=useragent.is_tablet,
			is_touch_capable=useragent.is_touch_capable,
			is_pc=useragent.is_pc,
			is_bot=useragent.is_bot,
			browser_family=useragent.browser.family,
			browser_version=useragent.browser.version_string,
			os_family=useragent.os.family,
			os_version=useragent.os.version_string,
			device_family=useragent.device.family)
		return ua
		
	except UserAgent.DoesNotExist:
		ua = UserAgent.objects.create(
			is_mobile=useragent.is_mobile, 
			is_tablet=useragent.is_tablet,
			is_touch_capable=useragent.is_touch_capable,
			is_pc=useragent.is_pc,
			is_bot=useragent.is_bot,
			browser_family=useragent.browser.family,
			browser_version=useragent.browser.version_string,
			os_family=useragent.os.family,
			os_version=useragent.os.version_string,
			device_family=useragent.device.family)
		return ua
		
		
# def real_data(sender, instance, action,  **kwargs):
# 	obj_count = AnalyticsReal.objects.all().count
# 	if obj_count>=10:
# 		analytics_view = AnalyticsReal.objects.all()
# 		if analytics_view is not None:
# 			for obj in analytics_view.all():
# 				analytics = AnalyticalData.objects.filter(slug=obj.slug, date=obj.datetime.date()).first()
# 				if analytics is not None:
# 					analytics.views +=1
# 					analytics.save()
# 				else:
# 					n = AnalyticalData()
# 					n.views =1
# 					n.slug = siteslug
# 					n.date = obj.datetime.date()
# 					n.save()
# 				obj.delete()

# m2m_changed.connect(real_data, sender=AnalyticsReal.slug.through)