from news.models import Category

from .models import FooterSettings, SidebarSettings
import re


def get_menu_items(site, category):
    childs = Category.objects.filter(site=site, parent=category)
    return {
        'cat_name'      : category.title,
        'cat_slug'      : category.slug.slug,
        'news_count'    : category.news_count,
        'childs'        : [get_menu_items(site, c) for c in list(childs) ],
    }

def get_menu_list(site):
    cat = Category.objects.filter(site=site, parent=None)
    return [get_menu_items(site, c) for c in list(cat)] 

def print_menu_item(menu, ischild=False):
    if len(menu['childs'])>0:
        return '<li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="/'+menu['cat_slug']+'" id="'+menu['cat_slug']+'" role="button" aria-haspopup="true" aria-expanded="false">'+ menu['cat_name'] +'</a> <div class="small dropdown-menu" aria-labelledby="'+menu['cat_slug']+'">'+ "".join([print_menu_item(m, True) for m in menu['childs']]) + '</div></li>'
    elif ischild:
            return '<a class="dropdown-item" href="/'+menu['cat_slug']+'">'+menu['cat_name']+'</a>'
    else:
        return '<li class="nav-item"><a class="nav-link" href="/'+menu['cat_slug']+'">'+ menu['cat_name'] +'</a></li>'

def print_menu_list(site):
    menu_string = ''
    menu_list = get_menu_list(site)
    
    for menu in menu_list[:5]:
        menu_string = menu_string + print_menu_item(menu)
        
    return menu_string
    
def get_parent_cat(cat):
    try:
        parent = cat.parent # Category.objects.filter(parent=cat).first()
        if parent is not None:
            return parent
        else:
            return False
    except Category.DoesNotExists:
        return False

def get_child_cat(cat):
    try:
        parent = Category.objects.filter(parent=cat)
        # print(parent)
        if parent:
            return parent
        else:
            print("no childs")
            return False
    except Category.DoesNotExists:
        print("no childs")
        return False
        
def get_child_cats(cat):
    br_string = ""
    children = get_child_cat(cat)
    if children is False:
        return ""
    else: 
        for child in children.all():
            
            br_string = br_string + '<a class="children" href="/'+child.slug.slug+'"><span class="children" >'+child.title+ '</span></a>'
        return br_string

def print_bread(cat):
    br_string = '<li class="breadcrumb-item"><a href="/'+cat.slug.slug+'">'+cat.title+'</a></li>'
    
    parent = get_parent_cat(cat)
    if parent is not False:
        return print_bread(parent) + br_string
    else:
        return '<li class="breadcrumb-item"><a href="/">Home</a></li>'+br_string
    
def print_breadcrumbs(news):
    if news is not None:
        cat = news.category.all()[0]
        return print_bread(cat)
    else:
        return ""
    
def get_row_count(sec):
    r = sec.split('R')[1].split("C")
    row = int(r[0])
    col = int(r[1])
    return row, col

def print_footer_section(site):
    sections = FooterSettings.objects.filter(site=site).order_by('section')
    html = ""
    size = ["col-sm-12","col-sm-6","col-sm-4"]
    row_count = [[0,0,0],[0,0,0]]
    footer_sections ={}
    for sec in sections:
        row, col= get_row_count(sec.section)
        footer_sections[sec.section]= sec
        row_count[row-1][col-1]= 1
        # print(sec.section)
        
    for row in range(0,2):
        if sum(row_count[row])>0:
            html = html+ '<div class="row">'
            col_size = size[sum(row_count[row])-1]
            for col in range(0,3):
                sec = "R{}C{}".format(row+1,col+1)
                if sec in footer_sections.keys():
                    html = html+ '<div class="{}"> <h4>'.format(col_size)+ footer_sections[sec].title+"</h4>"
                    html = html+  footer_sections[sec].content+"</div>"
                 
            html = html+"</div>"        
    # print(sum(row_count[0]))
    # print(sum(row_count[1]))
    return html
    
def print_sidebar_section(site):
    sections = SidebarSettings.objects.filter(site=site).order_by('index')
    html = ""
    for sec in sections:
        html = html +'''<div class="sidebar__item">
                               <h4>'''+sec.title+'''</h4>
                               '''+sec.content+'''
                             </div>'''
                             
    return html
    