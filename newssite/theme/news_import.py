from news.models import  News
from news.models import  Category
import requests
from urllib.parse import urlparse
from django.utils.html import strip_tags
from datetime import datetime
from django.contrib.auth.models import User

from django.core.files.base import ContentFile
from django.http import HttpResponse
from django.shortcuts import redirect


def get_image(url):
    name = urlparse(url).path.split('/')[-1]
    return name , ContentFile(requests.get(url).content)

def get_cat_names(categories, catids):
    names = []
    for cid in catids:
        for category in categories:
            if category['id'] == cid:
                names.append(category['name'])
    return names


def get_present_category(categories, lang, names):

    cats = []
    for name in names:
        found = False
        # for cat in categories:
        if Category.objects.filter(site=lang, title=name).exists():
            found = True
            req = Category.objects.filter(site=lang, title=name).first()
            cats.append(req)
        else:
            cats.append(add_category(name, lang))

    return cats

def add_category(name,lang):
    cat =  Category()
    cat.title = name
    cat.site = lang
    return cat.save()


def get_api_response(url):
    res = requests.get(url)
    return res.json()

def checknews(t):
    if News.objects.filter(title=t).exists():
        return False
    else:
        return True

def get_news(request):
    if not request.user.id is None:
        site = request.POST.get('site','') or request.GET.get('site','')
        status = request.POST.get('status','') or request.GET.get('status','')
        count = request.POST.get('count','') or request.GET.get('count','')
        page = request.POST.get('page','') or request.GET.get('page','')
        saved = request.POST.get('saved','') or request.GET.get('saved','')
        # cat_id = request.POST.get('cat','') or request.GET.get('cat','')
        if saved is "":
            saved = 0
        else:
            saved =  int(saved)

        if page is "":
            page = 1
        else:
            page =  int(page)

        if count is "":
            count = 10
        else:
            count =  int(count)

        if status is "":
            status = "published"

        if site is "":
            lang = "en"
            url = "https://kathmandutribune.com/"
        else:
            lang = "np"
            url = "https://nepalitribune.com/"

        # check if there is no site?
        # if there is add categry of news if its not there?
        # add news
        # fetch image
        # save everything
        site_categories = Category.objects.filter(site=lang)

        categories = get_api_response(url+"wp-json/wp/v2/categories?per_page=100")
        fetched_news = get_api_response(url+"wp-json/wp/v2/posts?per_page=10&page={}".format(page))
        print(len(fetched_news))
        n_fetched = 0
        for news in fetched_news:
            # print(news)
            if checknews(strip_tags(news['title']['rendered'])):

                print(type(news))
                print(n_fetched)
                cat_names = get_cat_names(categories, news['categories'])
                record = News()
                # record.id = news['id']
                record.title = strip_tags(news['title']['rendered'])
                print(get_present_category(site_categories, lang, cat_names))

                record.published = datetime.strptime(news['date'], "%Y-%m-%dT%H:%M:%S")
                record.updated = datetime.strptime(news['modified'], "%Y-%m-%dT%H:%M:%S")
                record.content = news['content']['rendered']
                record.author = User.objects.get(id=request.user.id)
                record.save()
                imgname , imagedata = get_image(news['jetpack_featured_media_url'])
                record.image.save(imgname, imagedata)
                record.category.set(get_present_category(site_categories, lang, cat_names))
                record.status = status
                record.save()

                n_fetched = n_fetched+1
        # return render(request, "comments/commentdisplay.html",context)
        # return HttpResponse("Total news added {}".format(n_fetched))
        if page*10 < count :
            return redirect('/news_import/?page={}&count={}&saved={}&site={}&status={}'.format(page+1,count,n_fetched+saved,site,status))
        else:
            return HttpResponse("{} Total news added".format(n_fetched+saved))
