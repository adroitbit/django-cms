from django import template
from django.template.defaultfilters import stringfilter

from theme.functions import print_menu_list, print_breadcrumbs, print_footer_section, print_sidebar_section
from datetime import datetime
from news.models import DEFINED_SITES_DICT
from django.contrib.auth.models import User

register = template.Library()

@register.filter(name='lower')
@stringfilter
def lower(value): # Only one argument.
    """Converts a string into all lowercase"""
    return value.lower()



@register.filter(name='getWriter')
def getWriter(user): # Only one argument.
    return user.get_full_name()
    
@register.filter(name='getAuthor')
def getAuthor(news):
    # print(news.author)
    user = User.objects.get(username=news.author)
    return user.get_full_name()

@register.filter(name='getMenu')
def getMenu(site):
    return print_menu_list(site)
    
    
@register.filter(name='getBreadCrumbs')
def getBreadCrumbs(news):
    return print_breadcrumbs(news)

@register.filter(name='get_news_time_ago')    
def get_news_time_ago(news):
    time_str = get_time_ago(news.published)
    print(time_str)
    print(news.status)
    if time_str == "" or not news.status == 'published':
        return "Unpublished Article"
    elif "ago" in time_str:
        return time_str
    else:
        return "Published on "+time_str
    
@register.filter(name='get_footer_sections')    
def get_footer_sections(site):
    return print_footer_section(site)
    
    
@register.filter(name='get_sidebar_sections')    
def get_sidebar_sections(site):
    return print_sidebar_section(site)
    
    
@register.filter(name='get_time_ago')
def get_time_ago(date):
    if date is not None:
            time = datetime.now()
            if date.hour == time.hour:
                return "Published "+ str(time.minute - date.minute) + " minutes ago"
            elif date.day == time.day:
                return "Published "+ str(time.hour - date.hour) + " hours ago"
            elif date.month == time.month:
                return "Published "+ str(time.day - date.day) + " days ago"
            else:
                return str(date.date())
    return ""

@register.filter(name='getNewsSite')
def getNewsSite(news):
    return DEFINED_SITES_DICT[news.get_news_site()]

@register.filter(name='getCatergory')
def getCatergory(cat):
    return cat.all()[0].title
