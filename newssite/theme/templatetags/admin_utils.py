from django import template
from django.template.defaultfilters import stringfilter

from datetime import datetime
from news.models import DEFINED_POST_STATUS_DICT

from usermgt.models import Activity

from usermgt.functions import check_user_assigned_for_review
register = template.Library()

@register.filter(name='getCategories')
def getCategories(category):
    html = ""
    first = True
    for c in category.all():
        if first:
            first= False
        else:
            html += ", "
        html += c.title 
    return html
    
    

@register.filter(name='getTags')
def getTags(category):
    html = ""
    first = True
    for c in category.all():
        if first:
            first= False
        else:
            html += ", "
        html += c.title 
    return html

@register.filter(name='getStatusString')
def getStatusString(status):
    return DEFINED_POST_STATUS_DICT[status]
    
@register.filter(name='isAssignedPostForReviewTo')
def isAssignedPostForReviewTo(user, post):
    obj  = Activity.objects.filter(ntuser__user = user,activity_type=3, item_id = post.id,item_type = 'post').first()
    # print(aobj)
    # print(user)
    # print(post)
    # print(obj.item_id)
    # print(obj.ntuser.user.username)
    try:
        if obj.ntuser.user.username == user.username:
            return True
        else:
            return False
    except:
        return False