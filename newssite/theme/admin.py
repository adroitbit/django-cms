from django.contrib import admin

from .models import PlaceHolder, PlaceCategory, SiteSettings, FooterSettings, SidebarSettings

class PlaceHolderAdmin(admin.ModelAdmin):
    list_display = ("title", 'stype','shown_page')


admin.site.register(PlaceHolder, PlaceHolderAdmin)

class CategoryPlaceAdmin(admin.ModelAdmin):
    list_display = ("cat","shown", "placeholder",'sindex')


admin.site.register(PlaceCategory, CategoryPlaceAdmin)

class SiteSettingsAdmin(admin.ModelAdmin):
    list_display = ("site","slug", "title","is_default")
    
admin.site.register(SiteSettings, SiteSettingsAdmin)


class FooterAdmin(admin.ModelAdmin):
    list_display = ("site", "title","section")
    
admin.site.register(FooterSettings, FooterAdmin)


class SidebarAdmin(admin.ModelAdmin):
    list_display = ("site", "title","index" )
    
admin.site.register(SidebarSettings, SidebarAdmin)
