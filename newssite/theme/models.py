from django.db import models

from news.models import Category

# from ckeditor.fields import RichTextField

# from django_summernote.admin import SummernoteModelAdmin

from news.models import DEFINED_SITES

class PlaceHolder(models.Model):
    type_choices= (
        ('ns','News Section'),
        ('ad','Advertisement'),
        ('sbad','Sidebar Advertisement'),
        ('sb','Sidebar'),
        ('mn','Menu'),
        ('sc','Showcase/Slider'),
        ('ha','Hideable content'),
        )
    page_choices = (
        ('home','Home'),
        ('category','Category'),
        ('news','News'),
        ('home_n_cat','Home and category'),
        ('news_n_cat','News and category'),
        ('all','Home, category and news'),

        )
    title= models.CharField(max_length=100, unique=True)
    details= models.CharField(max_length=200, default="", blank=True, null=True)
    stype = models.CharField(max_length=4, choices =type_choices,verbose_name = "Section Type", default = 'ns')
    shown_page = models.CharField(max_length=10, choices =page_choices,verbose_name = "Shown On page", default = 'home')
    sindex = models.PositiveIntegerField(default=1,verbose_name = "Section Index")
    def __str__(self):
        return self.title

# default_placeholders = [
#     {'title':"ad_top_head", "details":"","stype": "ad", "shown_page":"home", 'sindex':1 },
#     {'title':"site_menu", "details":"","stype": "mn", "shown_page":"home", 'sindex':1 },
#     {'title':"home_showcase", "details":"","stype": "sc", "shown_page":"home", 'sindex':1 },
#     {'title':"cat_showcase", "details":"","stype": "sc", "shown_page":"category", 'sindex':1 },
#     {'title':"featured_news", "details":"","stype": "ns", "shown_page":"home", 'sindex':1 },
#     {'title':"most_views_news", "details":"","stype": "ns", "shown_page":"home", 'sindex':999 },
#     {'title':"cat_news_slide", "details":"","stype": "ns", "shown_page":"home", 'sindex':999 },
#     {'title':"cat_news_normal", "details":"","stype": "ns", "shown_page":"home", 'sindex':999 },
#     {'title':"cat_news_half", "details":"","stype": "ns", "shown_page":"home", 'sindex':999 },
#     {'title':"ad_after_ns", "details":"","stype": "ad", "shown_page":"home", 'sindex':1 },
#     {'title':"ad_inside_ns_1", "details":"","stype": "ad", "shown_page":"home", 'sindex':1 },
#     {'title':"ad_inside_ns_2", "details":"","stype": "ad", "shown_page":"home", 'sindex':1 },
#     {'title':"ad_inside_ns_3", "details":"","stype": "ad", "shown_page":"home", 'sindex':1 },
#     ]

# def insert_initial_data(sender, app, created_models, verbosity, **kwargs):
#     if PlaceHolder in created_models:
#         for record in default_placeholders:
#             PlaceHolder.objects.get_or_create(title=record['title'],details=record['details'],stype=record['stype'],shown_page=record['shown_page'],sindex=record['sindex'])

# post_syncdb.connect(insert_initial_data)



class PlaceCategory(models.Model):
    class Meta:
        verbose_name_plural = "Category Places"
        verbose_name = "Category Place"
    cat = models.ForeignKey(Category, on_delete=models.CASCADE ,related_name ='place_cat', verbose_name = "Category")
    shown = models.BooleanField(verbose_name = "Category shown", default=False)
    placeholder = models.ForeignKey(PlaceHolder, on_delete=models.CASCADE ,related_name ='cat_show', verbose_name = "PlaceHolder")
    sindex = models.PositiveIntegerField(default=1,verbose_name = "Section Index")

class SiteSettings(models.Model):
    class Meta:
        verbose_name_plural = "Site Settings"
        verbose_name = "Site Setting"
    site = models.CharField(max_length=4, choices =DEFINED_SITES, default = 'en', unique=True,verbose_name = "Site Language")
    title = models.CharField(max_length=50, unique=True,verbose_name = "Site Title")
    is_default = models.BooleanField(verbose_name = "Default Site", default=False)
    slug= models.CharField(max_length=100, unique=True)
    header = models.ImageField(upload_to='header/%Y/%m/%d/',blank=True,null=True,  default = None, verbose_name = "Site Header Image")
    logo = models.ImageField(upload_to='logo/%Y/%m/%d/',blank=True,null=True,  default = None, verbose_name = "Site Logo Image")
    favicon = models.ImageField(upload_to='logo/%Y/%m/%d/',blank=True,null=True,  default = None, verbose_name = "Site Favicon Image")
    ns_ad_count = models.PositiveIntegerField(default=1,verbose_name = "Ad after Sections ")
    def __str__(self):
        return self.title



class SidebarSettings(models.Model):
    class Meta:
        verbose_name_plural = "Sidebar Settings"
        verbose_name = "Sidebar Setting"
        unique_together = ('site', 'index',)
    site = models.CharField(max_length=4, choices =DEFINED_SITES, default = 'en',verbose_name = "Site")
    index =  models.PositiveIntegerField(default=1,verbose_name = "Index")
    title = models.CharField(max_length=50, verbose_name = "Section Title")
    content = models.TextField()


    def __str__(self):
        return self.title + "("+self.site+")"


class FooterSettings(models.Model):
    FOOTER_SECTIONS = (
        ('R1C1','First row first column'),
        ('R1C2','First row second column'),
        ('R1C3','First row third column'),
        ('R2C1','Second row first column'),
        ('R2C2','Second row second column'),
        ('R2C3','Second row third column'),
        )
    class Meta:
        verbose_name_plural = "Footer Settings"
        verbose_name = "Footer Setting"
        unique_together = ('site', 'section',)
    site = models.CharField(max_length=4, choices =DEFINED_SITES, default = 'en', verbose_name = "Site")
    section = models.CharField(max_length=6, choices =FOOTER_SECTIONS, default = 'FS1',verbose_name = "Section")
    title = models.CharField(max_length=50, verbose_name = "Section Title")
    content = models.TextField()
    def __str__(self):
        return self.title + "("+self.site+")"
