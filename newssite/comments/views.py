from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.http import JsonResponse

from comments.models import Comment
from news.models import News
from .forms import CommentForm
# from comments.functions import comments_list, post_comments,get_latest_comments,get_comment_child
from .functions import get_parent_comments, get_child_comments

from django.utils.html import escape
from django.utils.html import strip_tags
from django.core.paginator import Paginator
from .serializers import ChildSerializer
from rest_framework.response import Response
from rest_framework.views import APIView 

from .functions import get_comment_json
from .const import COMMENTS_PER_REQUEST

def model_comment_view(request):
        res = {"success":False,"comment":None}
        if request.user.is_authenticated:
                form = CommentForm(request.POST)
                if form.is_valid():
                        comment = form.save()
                        res['comment'] = get_comment_json(comment)
                        res['success']=True
                        print(comment)
                        
        else:
                print("not authenticated")
        
        return JsonResponse(res)
        
def model_get_comment(request):
        res = {"comment":None,"maxval":0,"success":False}
        if request.method == "GET":
                news_id = int(request.GET.get('news',0))
                page = int(request.GET.get('page',0))
        elif request.method == "POST":
                news_id = int(request.POST.get('news',0))
                page = int(request.POST.get('page',0))
        try:
                news = News.objects.get(id=news_id)
                # print(news)
                comments , comment_count , crange= get_parent_comments(news=news,page=page, count=COMMENTS_PER_REQUEST)
                print(comment_count)
                print(crange)
                res["comment"] =comments
                res["maxval"] = len(crange)
                res['success']=True
                return JsonResponse(res)
        except News.DoesNotExist:    
                return JsonResponse(res)

def model_get_child_comment(request):
        res = {"comment":None,"maxval":0,"success":False}
        if request.method == "GET":
                news_id = int(request.GET.get('news',0))
                parent_id = int(request.GET.get('parent',0))
                page = int(request.GET.get('page',0))
        elif request.method == "POST":
                news_id = int(request.POST.get('news',0))
                parent_id = int(request.POST.get('parent',0))
                page = int(request.POST.get('page',0))
        # print(parent_id)
        # print(news_id)
        # print(page)
        try:
                parent = Comment.objects.get(id=parent_id)
                # print(news)
                comments , crange= get_child_comments(parent=parent,page=page, count=COMMENTS_PER_REQUEST)
                # print(comment_count)
                print(crange)
                res["comment"] =comments
                res["maxval"] = len(crange)
                res['success']=True
                return JsonResponse(res)
        except Comment.DoesNotExist:    
                return JsonResponse(res)

# def comment_template(request):
#     if request.method == "GET":
#         obj = get_latest_comments()
#         paginator = Paginator(obj,2)
#         page = request.GET.get('page','')
#         obj_page = paginator.get_page(page)
#         p_obj = get_comment_child(obj_page)

#         context = {
#         'object': p_obj
#         }
#     return render(request, "comments/commentdisplay.html",context)

# class child_view(APIView):
#     def get(self,request):
#         if request.method == "GET":
#             obj_cmt = get_latest_comments()
#             c_obj = get_child_of(obj_cmt)
#             print(c_obj)
#             child_of = request.GET.get('child_of','')
#             print(child_of)
#             # obj_child = Comment.objects.filter(comments_text=obj_cmt)
#             # print(obj_child)
#             serializer = ChildSerializer(c_obj,many=True)
#             return Response(serializer.data)
        # return render(request,"")
                # child_id = request.GET.get('child_of','')
        # obj_child = Comment.objects.get(child_id=child_of)
        # print(obj_child)