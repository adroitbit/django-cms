from rest_framework import serializers
from comments.models import Comment

class ChildSerializer(serializers.ModelSerializer):
	comments_text = serializers.CharField(max_length=160)
	datetime =  serializers.DateTimeField()
	class Meta:
		model = Comment
		fields = ['datetime','comments_text','user']