from django import forms

from .models import Comment



	# comments_text = forms.CharField(label='comment',
	#  widget=forms.Textarea(attrs=
	#  	{"placeholder":"Comment here",
	#  	"required": True,
	#  	"class":"w-100", 
	#  	 "rows":3,
	#  	  "cols":83}))
	# news_id = forms.CharField(max_length=150,required= True, widget=forms.TextInput(attrs=
	# 	{
	# 	"hidden": True,
	# 	}))
	# parent_id = forms.CharField(max_length=150,required= False, widget=forms.TextInput(attrs=
	# 	{
	# 	"hidden":True
	# 	}) 
	# 	)
class CommentForm(forms.ModelForm):	
	class Meta():
		comments_text = forms.CharField(label='comment')
		model = Comment
		fields =  ('comments_text','news','user','parent')
		widgets = {
        	'comments_text': forms.Textarea(attrs={"placeholder":"Comment here", "required": True, "class":"w-100", "rows":3, "cols":83}),
            'news': 	forms.HiddenInput(),
            'parent':	forms.HiddenInput(),
            'user':		forms.HiddenInput(),
		}
            