from django.db import models
from news.models import News
from django.contrib.auth.models import User

# Create your models here.
class Comment(models.Model):
    class Meta:
        verbose_name_plural = "Comments"
    
    user = models.ForeignKey(User, on_delete=models.CASCADE ,related_name ='user', verbose_name = "User")
    comments_text = models.TextField(verbose_name = "Comment Text")
    news = models.ForeignKey(News, on_delete=models.CASCADE, related_name ='news_comments')
    approved = models.BooleanField(default=False)
    parent = models.ForeignKey('self', verbose_name = "Parent Comment", blank=True, null=True, on_delete=models.CASCADE,related_name='child_of')
    datetime =  models.DateTimeField(auto_now=True, verbose_name = "Comment posted on")

    def __str__(self):
        return self.comments_text
    
    # reply = models.ForeignKey('Comment',on_delete=models.CASCADE, null=True,related_name="replies")
    
    

     