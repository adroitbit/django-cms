from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
# from django.shortcuts import get_object_or_404

from comments.models import Comment
from news.models import News
from .forms import CommentForm
from django.core.paginator import Paginator
from usermgt.models import NTUser
from django.contrib.staticfiles.templatetags.staticfiles import static
# from django.templatetags.static import static
# from django.contrib.staticfiles.storage import staticfiles_storage
# from django.templatetags.static import static

from django.conf import settings
# from django.conf.urls.static import static
# def post_comments():
    
#     comments_text = request.POST.get('comments_text')
#     Comment.objects.create(comments_text=comments_text,news=obj,user=user)
    
#     return user,comments_text
    
    # print(type(request.user))
    # if request.method == "POST":
    #     pid = request.POST.get('news_id', '')
    #     try:
    #         obj = News.objects.get(id=pid)
    #     except News.DoesNotExist:
    #         obj = None
        
    #     if request.user.is_authenticated:
            
    #         user = User.objects.get(id=request.user.id)
    #         comments_text = request.POST.get('comments_text')
    #         Comment.objects.create(comments_text=comments_text,news=obj,user=user)
    #     else:
    #         return render(request, "registration/login.html")
    # return render(request, "comments/comments.html")
    
    
def post_comments(news_id, comments_text,parent, user):
    try:
        obj = News.objects.get(id=news_id)
        if parent == 'null' or parent == '' :
            Comment.objects.create(comments_text=comments_text, news=obj,user=user)
        else:
            Comment.objects.create(comments_text=comments_text, parent=parent,news=obj,user=user)
        return True
    except:
        return False
    return user,comments_text
    
def parent_comments_list(news):
    comments_all = Comment.objects.filter(news=news, parent=None).order_by('-datetime').all()
    count = Comment.objects.filter(news=news, parent=None).count()
    # print( Comment.objects.filter(news=news, parent=None).count())
    # print(comments_all)
    return comments_all, count

def get_parent_comments(news, page = 1,count=10):
    # print(news)
    # print("called get_parent_comments")
    comments, tcount = parent_comments_list(news)
    # print(comments)
    comment_wcount = [{'counts':p.child_of.all().count(), 'comment_obj':get_comment_json(p)} for p in comments ]
    paginator = Paginator(comment_wcount, count)
    pobj = paginator.page(page)
    # print(pobj.object_list)
    return pobj.object_list, tcount ,paginator.page_range

def get_child_comments(parent, page = 1,count=10):
    c_comments = Comment.objects.filter(parent=parent).order_by('-datetime')
    comment_wcount = [{'counts':p.child_of.all().count(), 'comment_obj':get_comment_json(p)} for p in c_comments ]
    paginator = Paginator(comment_wcount, count)
    pobj = paginator.page(page)
    return pobj.object_list, paginator.page_range
    

def get_comment_json(comment):
    ntuser = NTUser.objects.get(user =comment.user)
    if ntuser.profile_pic:
        return {                "id": comment.id,
                                "comments_text":comment.comments_text,
                                "time":comment.datetime,
                                "user" :comment.user.get_full_name(),
                                "username":comment.user.username,
                                "parent": comment.parent.id if comment.parent is not None else None,
                                "image" : ntuser.profile_pic.url
                                
                        }
    else:
        return {                "id": comment.id,
                                "comments_text":comment.comments_text,
                                "time":comment.datetime,
                                "user" :comment.user.get_full_name(),
                                "username":comment.user.username,
                                "parent": comment.parent.id if comment.parent is not None else None,
                                "image" :  static("site/img/user.png")
                                    
                                    # staticfiles_storage.url("site/img/user.png")
                                
                        }