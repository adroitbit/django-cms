from django.shortcuts import render

from news.models import SiteSlug, News, Tag, Category
from seo.models import SiteSEO,SiteDependent
# Create your views here.
def seo_site_view(request):
	slug = request.POST.get('slug','') or request.GET.get('slug','') 
	print(slug)
	try:
		siteslug = SiteSlug.objects.get(slug=slug)
		print(siteslug.model)	
	except SiteSlug.DoesNotExist:
		siteslug = None
	
	if request.method == "POST":
		if siteslug.model == 'news':
			news = News.objects.filter(slug=siteslug).first()
			# print(news.slug)
			news_slug = news.slug
			print(news_slug)
			title = request.POST.get('title')   
			description = request.POST.get('description')
			keywords = request.POST.get('keywords')
			SiteSEO.objects.create(slug=news_slug,title=title,description=description,keywords=keywords)
		elif siteslug.model == 'tag':
			tag = Tag.objects.get(slug=siteslug).first()
			tag_slug = tag.slug
			print(tag_slug)
			title = request.POST.get('title')   
			description = request.POST.get('description')
			keywords = request.POST.get('keywords')
			SiteSEO.objects.create(slug=tag_slug,title=title,description=description,keywords=keywords)
		elif siteslug.model == 'cat':
			print("hi")
			cat= Category.objects.get(slug=siteslug).first()
			cat_slug = cat.slug
			print(cat_slug)
			title = request.POST.get('title')  
			print(title) 
			description = request.POST.get('description')
			keywords = request.POST.get('keywords')
			print(keywords) 
			SiteSEO.objects.create(slug=cat_slug,title=title,description=description,keywords=keywords)
	return render(request,"seo/seo.html", {'slug':slug})

def site_description_view(request,locale):
	if request.method == "POST":
		locale = request.POST.get('locale')
		site_name = request.POST.get('site_name')   
		article_publisher = request.POST.get('article_publisher')
		fb_app_id = request.POST.get('fb_app_id')
		fb_page_link = request.POST.get('fb_page_link')
		twitter_site = request.POST.get('twitter_site')
		twitter_handle = request.POST.get('twitter_handle')
		google_site_verification = request.POST.get('google_site_verification')
		msvalidate = request.POST.get('msvalidate')
		yandex_verification = request.POST.get('yandex_verification')
		SiteDependent.objects.create(locale=locale,site_name=site_name,article_publisher=article_publisher,fb_app_id=fb_app_id,twitter_site=twitter_site,google_site_verification=google_site_verification,msvalidate=msvalidate,yandex_verification=yandex_verification)
	return render(request,"seo/seodependent.html", {'locale':locale})