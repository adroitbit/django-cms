from .models import SiteSEO, SlugSEO
from django.utils.html import escape
from django.utils.html import strip_tags
from django.conf import settings

from django.utils.text import Truncator
def getSiteSEOtags(site):

    html = ""
    seobj = SiteSEO.objects.filter(site=site).first()
    if seobj  and seobj.site_name is not None:
        html = html+'<meta property="og:site_name" content="'+ seobj.site_name+'" />\n'
    if seobj  and seobj.article_publisher is not None:
        html = html+'<meta property="article:publisher" content="'+ seobj.article_publisher+'" />\n'
    if seobj  and seobj.fb_app_id is not None:
        html = html+'<meta property="fb:app_id" content="'+ seobj.fb_app_id+'" />\n'
    if seobj  and seobj.fb_page_link is not None:
        html = html+'<meta property="fb:page_link" content="'+ seobj.fb_page_link+'" />\n'
    if seobj  and seobj.twitter_site is not None:
        html = html+'<meta name="twitter:site" content="'+ seobj.twitter_site+'"  />\n'
    if seobj  and seobj.twitter_handle is not None:
        html = html+'<meta name="twitter:handle" content="'+ seobj.twitter_handle+'" />\n'
    if seobj  and seobj.google_site_verification is not None:
        html = html+'<meta name="google-site-verification" content="'+ seobj.google_site_verification+'" />\n'
    if seobj  and seobj.msvalidate is not None:
        html = html+'<meta name="msvalidate"  content="'+ seobj.msvalidate+'" />\n'
    if seobj  and seobj.yandex_verification is not None:
        html = html+'<meta name="yandex_verification"  content="'+ seobj.yandex_verification+'" />\n'

    return html


def getSlugSEOtags(slug,item):
    slugobj = SlugSEO.objects.filter(slug=slug).first()

    html = ""
    title = ""
    desc = ""
    updated = ""
    author = ""
    img = ""
    if slugobj and slugobj.title:
        title = slugobj.title
    else:
        title = item.title

    if slugobj and slugobj.description:
        desc =slugobj.description
    elif (slug.model == 'cat' or slug.model == 'tag') and item.description:
        desc = item.description
    elif slug.model == 'news' and item.content :
        trunc = Truncator(strip_tags(item.content))
        desc = trunc.words(20,html=True)

    if slug.model == 'news':
        updated = str(item.updated)
        author = item.author.get_full_name() or item.author.username
    if slug.model == 'news' and  item.image:
        img = settings.DOMAIN_NAME +item.image.url

    html = html + '<meta property="og:title" content="'+ title+'" />\n'
    html = html + '<meta property="og:description" content="'+ desc+'"/>\n'
    html = html + '<meta property="og:url" content="'+settings.DOMAIN_NAME +'/'+slug.slug+'" />'

    if slugobj is not None:
        html = html + '<meta property="og:keywords" content="'+ slugobj.keywords+'" />\n'

    if slug.model == 'news':
        html = html +'<meta property="og:image" content="'+img+'"/>\n'
        html = html +'<meta property="og:updated_time" content="'+updated+'"/>\n'
        html = html +'<meta property="article:author" content="'+author+'"/>\n'
    return html
