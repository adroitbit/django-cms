from django.apps import AppConfig


class SeoConfig(AppConfig):
    name = 'Search Engine Optimization'
