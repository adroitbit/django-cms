from django.contrib import admin

from seo.models import SiteSEO,SlugSEO
# Register your models here.
admin.site.register(SiteSEO)
admin.site.register(SlugSEO)