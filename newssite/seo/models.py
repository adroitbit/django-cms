from django.db import models
from news.models import SiteSlug
from news.models import DEFINED_SITES
# Create your models here.

class SlugSEO(models.Model):
    class Meta:
        verbose_name_plural = "SlugSEO"
        verbose_name = "SlugSEO"
    title = models.CharField(max_length=250,unique=True)
    slug = models.ForeignKey(SiteSlug,on_delete=models.CASCADE)
    description = models.CharField(max_length=250,blank=True,null=True,  default = None)
    keywords = models.CharField(max_length=250,blank=True,null=True,  default = None)

class SiteSEO(models.Model):
    class Meta:
        verbose_name_plural = "SiteSEO"
        verbose_name = "SiteSEO"
    site = models.CharField(max_length=4, choices =DEFINED_SITES, default = 'en', unique=True,verbose_name = "Site Language")
    locale = models.CharField(max_length=250,unique=True)
    site_name = models.CharField(max_length=250,unique=True)
    article_publisher = models.CharField(max_length=250,blank=True,null=True,  default = None)
    fb_app_id = models.CharField(max_length=250,blank=True,null=True,  default = None)
    fb_page_link = models.CharField(max_length=250,blank=True,null=True,  default = None)
    twitter_site = models.CharField(max_length=250,blank=True,null=True,  default = None)
    twitter_handle = models.CharField(max_length=250,blank=True,null=True,  default = None)
    google_site_verification = models.CharField(max_length=250,blank=True,null=True,  default = None)
    msvalidate = models.CharField(max_length=250,blank=True,null=True,  default = None)
    yandex_verification = models.CharField(max_length=250,blank=True,null=True,  default = None)