from django.db import models

from django.contrib.auth.models import User

# Create your models here.
class AddModel(models.Model):
	publisher = models.ForeignKey(User, on_delete=models.CASCADE,verbose_name = "User")
	advertisement = models.CharField(verbose_name = "Advertisement",max_length=150)
	views = models.PositiveIntegerField(default=0)
	url = models.URLField(max_length=200)
	starttime = models.DateTimeField(null=True)
	endtime = models.DateTimeField(null=True)
	verified = models.BooleanField(default=True)
	verification_comments = models.CharField(verbose_name = "Verification Comments",max_length=120)
	

class Clicks(models.Model):
	# click = models.IntegerField(default=0 ,verbose_name = "Click")
	# views = models.IntegerField(default=0 ,verbose_name = "Views")
	advertisement_id = models.CharField(verbose_name = "Advertisement",max_length=150)
	user_agent = models.CharField(verbose_name = "User Agent", max_length=20)
	datetime =  models.DateTimeField(auto_now=True, verbose_name = "Advertise posted")