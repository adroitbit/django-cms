from django.shortcuts import render
from AdMgmt.models import AddModel,Clicks
from django.contrib.auth.models import User
# Create your views here.
def addmodel_view(request):
	if request.method == "POST":
		if request.user.is_authenticated:
			user = User.objects.get(id=request.user.id)
			print(user)
			advertisement = request.POST.get('advertisement')
			url = request.POST.get('url')
			AddModel.objects.create(advertisement=advertisement,publisher=user,url=url)
		else:
			return render(request, "registration/login.html")
	return render(request, "admgmt/add.html")

def click_view(request):
	if request.method == "GET":
		advertisement_id = request.GET.get('advertisement_id','')
		try: 
			ad = AddModel.objects.get(id=1)
			print(ad.id)
			ad.views += 1
			ad.save()
			ua = request.META.get('HTTP_USER_AGENT', '')
			s = Clicks()
			s.advertisement_id = ad.id
			s.user_agent = ua
			s.save()
		except AddModel.DoesNotExist:
			ad = None

		context = {
		's':s
		}


	return render(request,"admgmt/click.html",context)