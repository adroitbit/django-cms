// https://cookieconsent.insites.com/download/
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#3937a3"
    },
    "button": {
      "background": "#e62576"
    }
  },
  "theme": "classic",
  "position": "bottom-left",
  "content": {
    "message": "This website uses cookies to ensure you get the best experience on our website.",
    "dismiss": "ok",
    "link": "Learn more",
    "href": "example.com"
  }
})});
