$(function(){
    "use strict";

    //Activate main slider
    $("#featured").carousel({
       interval: 3000,
       pause:"hover",
       keyboard:true
   });


   //////////////////// Search JS
    $('a[href="#search"]').on('click', function(event) {
        event.preventDefault();
        $('#search').addClass('open');
        $('#search > form > input[type="search"]').focus();
    });

    $('#search, #search button.close').on('click keyup', function(event) {
        if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
            $(this).removeClass('open');
        }
    });

    // putting this code to make paypal donate button work
    // $('.smart-sidebar').on( 'click', '[name="submit"]', function(event) {
    //     $(this).closest("form").submit();
    // } );



/* scroll to top code*/



var amountScrolled = 300;

$(window).scroll(function() {
	if ( $(window).scrollTop() > amountScrolled ) {
		jQuery('a.back-to-top').fadeIn('slow');
	} else {
		jQuery('a.back-to-top').fadeOut('slow');
	}
});

$('a.back-to-top').click(function() {
	$('html, body').animate({
		scrollTop: 0
	}, 1000);
	return false;
});


/*
 *      Shrink menu when its scrolled down and static
 *
 */


var amountmenuScrolled = 200;
$(window).scroll(function() {
	if ( $(window).scrollTop() > amountmenuScrolled ) {
		$('header .navbar.bar-default').addClass('navbar-fixed-top');
    $('header .navbar.bar-default').addClass('inbody');
    $('header .navbar.bar-default').addClass('fixed-top');

	} else {
            $('header .navbar.bar-default').removeClass('navbar-fixed-top');
            $('header .navbar.bar-default').removeClass('inbody');
            $('header .navbar.bar-default').removeClass('fixed-top');

    }

});
function initCarousel(){
  if($('.featured-carousel')){
    if($(window).width()>992 ){
      $('.featured-carousel').owlCarousel('destroy');
      $('.owl-container').removeClass('owl-carousel');
      $('.main-section').removeClass('slide-active');
    }else {
      $('.owl-container').addClass('owl-carousel');
      $('.main-section').addClass('slide-active');
      $(".featured-carousel").owlCarousel();
    }

  }

}
function init_sidebar() {
// https://github.com/abouolia/sticky-sidebar
  if($('.smart-sidebar').length){
    // var sidebar = new StickySidebar('.sticky-sidebar', {
    //   topSpacing: 20,
    //   bottomSpacing: 20,
    //   containerSelector: '.main-content',
    //   innerWrapperSelector: '.sidebar__inner'
    // });
    if ( $(window).width() > 991 ) {
      $('.smart-sidebar').addClass('sticky-sidebar');
    //   $('.sticky-sidebar').stickySidebar({
    //     topSpacing: 60,
    //     bottomSpacing: 60,
    //     minWidth: 992
    // });
    // if ( window.innerWidth < 768 ) { sidebar.destroy(); }
    //  else if( window.innerWidth > 768 )
    //  { sidebar.updateSticky(); }

  } else if ( $('.sticky-sidebar').length ){

      // $('.sticky-sidebar').stickySidebar().destroy();
      $('.smart-sidebar').removeClass('sticky-sidebar');
      }


  }
}



// function initSectionCarousel(){
// $(".post-carousel").owlCarousel({
//     loop:true,
//     responsiveClass:true,
//     autoWidth:true,
//     responsive:{
//         0:{
//             items:2,
//             nav:true
//         },
//         600:{
//             items:3,
//             nav:false
//         },
//         1000:{
//             items:4,
//             nav:true,
//             loop:false
//         }
//     },
//     nav:true,
// });
// }

function initRating() {


}

$(document).ready(function(){
  initCarousel();
  init_sidebar();

  console.log("initialize star");
  //http://plugins.krajee.com/star-rating-demo-theme-uni
  //https://github.com/kartik-v/bootstrap-star-rating
   if ( $('.kv-ltr-theme-uni-star').length ){
           $(".kv-ltr-theme-uni-star").rating({
          min:0,
          max:5,
          step:0.5,
          hoverOnClear: false,
          theme: 'krajee-uni',
          starCaptions:{
          0.5: '0.5',
          1: '1',
          1.5: '1.5',
          2: '2',
          2.5: '2.5',
          3: '3',
          3.5: '3.5',
          4: '4',
          4.5: '4.5',
          5: '5'
      }
        });
        $('.kv-ltr-theme-uni-star').on('rating:change', function(event, value, caption) {
          console.log(value);
          // console.log(caption);
          change_rating(value);
      });
   }
  // initSectionCarousel();
});

function change_rating(value){
  var user = $('#id_user').val();
  var rating = parseFloat(value);
  var news = $('#id_news').val();
  var csrfmiddlewaretoken = $('#addRatingForm input[name="csrfmiddlewaretoken"]').val();
  var data = {'user':user,
              'rating_s':rating,
              'news':news, 
              "csrfmiddlewaretoken":csrfmiddlewaretoken,};
  console.log(data);
  $.ajax({
    type:"POST",
    url:"/add_ratings/",
    data:data,
    dataType: "json",
    success: function(resp){
      console.log(resp);
      if(resp.success){
        console.log("success");
        // $('.rating_s').val("");
      }
      else{
        console.log("nothing")
      }
    }
  });
}


$(window).resize(function(){
  initCarousel();
  init_sidebar();
});

});
