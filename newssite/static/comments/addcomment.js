
$(function(){
    "use strict";
    function escapeHtml(html){
      var text = document.createTextNode(html);
      var p = document.createElement('p');
      p.appendChild(text);
      return p.innerHTML;
    }
    function get_comment_html(comment,isparent,token){
        var html ="", parcss ="";
        if (isparent){
            parcss="comment-single parent";
        }else{
            parcss="comment-single child";
        }

        html =html + '<div class="'+parcss+'"><div class="row"><div class="col-2 profile"><img src="'+comment.comment_obj.image+'" width="50px"/></div><div class="col"><div class="row">';
        if(comment.comment_obj.user==""){
            html =html + comment.comment_obj.username;
        }else{
            html =html + comment.comment_obj.user;
        }
        html =html + '</div><div class="row comment"><div class="col">'+ escapeHtml(comment.comment_obj.comments_text);
        html =html + '</div></div><div class="row reply"><a href="#" data-id="'+comment.comment_obj.id+'" id="comment_reply_'+comment.comment_obj.id+'" class="comment-reply">Reply</a>';
        html =html + '</div></div></div></div>';
        console.log(comment.counts);
        if(comment.counts>0){
            html =html + '<div class="load-reply comment text-center"><form id="getReplyForm_'+comment.comment_obj.id+'" action="" method="POST">'+'<input type="hidden" name="csrfmiddlewaretoken" value="'+token+'">';
            html =html + '<a class="load_replies comment-previous" data-page="1" data-id="'+comment.comment_obj.id+'" href="#" >View Replies</a> </form> </div> <div class="reply-buffer_'+comment.comment_obj.id+'"> </div>';
        }

        return html
    }
    function add_more_comments(comments,token){
        console.log(comments)

        for (var i=comments.length-1;i>=0;i--){
            var div = document.createElement( "div" );
            var html = get_comment_html(comments[i],true,token);
            div.innerHTML=html;
            $(".more-buffer").prepend($(div));
        }

    }

    function add_reply_comments(comments,token, id){
        console.log(comments);
        for (var i=comments.length-1;i>=0;i--){
            var div = document.createElement( "div" );
            var html = get_comment_html(comments[i],false,token);
            div.innerHTML=html;
            $(".reply-buffer_"+id).prepend($(div));
        }

    }

    $("#addcommentButton").on('click', function(event) {
        event.preventDefault();
        var par = $('#id_parent').val();
        var user = $('#id_user').val();
        var news = $('#id_news').val();
        var comment = $('#id_comments_text').val();
        var csrfmiddlewaretoken = $('#addCommentForm input[name="csrfmiddlewaretoken"]').val();
        var data ={'user':user,'parent':par,'user':user, 'news':news,'comments_text':comment, 'csrfmiddlewaretoken':csrfmiddlewaretoken};
        console.log(data);

        $.ajax({
                 type:"POST",
                 url:"/addcomment/",
                 data: data,
                 success: function(data){
                     if(data.success){
                         console.log(data);
                         $('#id_comments_text').val("");
                     }else{
                         console.log("Something went wrong")
                     }
                    }
            });

    });

    // $(".comment-reply").on('click', function(event) {
    //     event.preventDefault();
    //     var newparent = $(this).attr("data-id");
    //     console.log(newparent);
    //     $('#id_parent').val(newparent);
    //     var par = $('#id_parent').val();
    //     console.log("new parent value "+par);
    //     $("#id_comments_text").focus();
    // });

    $(document).on( 'click', ".comment-reply", function(event) {
        event.preventDefault();
        var newparent = $(this).attr("data-id");
        console.log(newparent);
        $('#id_parent').val(newparent);
        var par = $('#id_parent').val();
        console.log("new parent value "+par);
        $("#id_comments_text").focus();
    } );

    $(".load_previous").on('click', function(event) {
        event.preventDefault();
        var parent_element = $(this);
        var page_c = parseInt(parent_element.attr("data-page"));
        var newsid = parseInt($('#id_news').val());
        var csrfmiddlewaretoken = $('#getCommentForm input[name="csrfmiddlewaretoken"]').val();
        var data = {
            "page":page_c+1,
            "news":newsid,
            "csrfmiddlewaretoken": csrfmiddlewaretoken,
        };
        console.log(data);
        $.ajax({
                 type:"POST",
                 url:"/getcomment/",
                 data: data,
                 success: function(res){
                     if(res.success){
                         console.log(res);

                         if(page_c+1 == res.maxval){
                             parent_element.html("");
                         }else{
                            parent_element.attr("data-page", page_c+1)
                         }
                         add_more_comments(res.comment,csrfmiddlewaretoken);
                     }else{
                         console.log("Something went wrong")
                     }
                    }
            });
    });

$(document).on( 'click', ".load_replies", function(event) {
        event.preventDefault();
        var parent_element = $(this);
        var page_c = parseInt(parent_element.attr("data-page"));
        var cid = parseInt(parent_element.attr("data-id"));
        var newsid = parseInt($('#id_news').val());
        var csrfmiddlewaretoken = $('#getReplyForm_'+cid+' input[name="csrfmiddlewaretoken"]').val();
        var data = {
            "page":page_c,
            "news":newsid,
            "csrfmiddlewaretoken": csrfmiddlewaretoken,
            "parent":cid,
        };
        console.log(data);
        $.ajax({
                 type:"POST",
                 url:"/getchid_comments/",
                 data: data,
                 success: function(res){
                     if(res.success){
                         console.log(res);

                         if(page_c == res.maxval){
                             parent_element.html("");
                         }else{
                            parent_element.attr("data-page", page_c+1)
                         }
                         add_reply_comments(res.comment, csrfmiddlewaretoken, cid);
                     }else{
                         console.log("Something went wrong")
                     }
                    }
            });
    } );


    // $(".load_replies").on('click', function(event) {
    //     event.preventDefault();
    //     var parent_element = $(this);
    //     var page_c = parseInt(parent_element.attr("data-page"));
    //     var cid = parseInt(parent_element.attr("data-id"));
    //     var newsid = parseInt($('#id_news').val());
    //     var csrfmiddlewaretoken = $('#getReplyForm_'+cid+' input[name="csrfmiddlewaretoken"]').val();
    //     var data = {
    //         "page":page_c,
    //         "news":newsid,
    //         "csrfmiddlewaretoken": csrfmiddlewaretoken,
    //         "parent":cid
    //     };
    //     console.log(data);
    //     $.ajax({
    //              type:"POST",
    //              url:"/getchid_comments/",
    //              data: data,
    //              success: function(res){
    //                  if(res.success){
    //                      console.log(res);

    //                      if(page_c == res.maxval){
    //                          parent_element.html("");
    //                      }else{
    //                         parent_element.attr("data-page", page_c+1)
    //                      }
    //                      add_reply_comments(res.comment, csrfmiddlewaretoken, cid);
    //                  }else{
    //                      console.log("Something went wrong")
    //                  }
    //                 }
    //         });
    // });

});
