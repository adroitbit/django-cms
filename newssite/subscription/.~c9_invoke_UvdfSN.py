from django import forms

from .models import subscr

class SubscritionForm(forms.ModelForm):
    email = forms.EmailField()
    name = forms.TextField()
    
    class Meta:
        model = subscription_model
        field = [
            '']