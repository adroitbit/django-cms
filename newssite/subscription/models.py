from django.db import models

# Create your models here.
class Subscription_model(models.Model):
    email = models.EmailField()
    name = models.CharField(max_length=120)
    category = models.CharField(max_length=120)
    # choices = (
    #     ('category 1','category 1'),
    #     ('category 2','category 2'),
    #     ('category 3','category 3'),
    #     ('category 4','category 4'),
    #     )
    # category = models.CharField(max_length = 100, choices = choices)