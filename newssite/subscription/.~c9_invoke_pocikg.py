from

from .models import subscrition_model

class SubscritionForm(forms.ModelForm):
    email = forms.EmailField()
    name = forms.TextField()
    
    class Meta:
        model = subscription_model
        field = [
            '']