from django import forms

from .models import Subscription_model

class SubscriptionForm(forms.ModelForm):
    
    class Meta:
        model = Subscription_model
        
        fields = [
            'email',
            'name',
            
            ]

# class SubscriptionForm(forms.Form):
#     email = forms.EmailField()
#     name = forms.CharField()
#     like = forms.ChoiceField(???)