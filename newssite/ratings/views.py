from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from ratings.models import Rating
from news.models import News
from django.http import JsonResponse
from ratings.forms import RatingForm

# Create your views here.
def ratings_view(request):
	if request.method == "POST":
		pid = request.POST.get('news_id', '')
		res = {"success":False,"rating":None,"auth":True,"error":False}
		if request.user.is_authenticated:
				# form = RatingForm(request.POST)
				newsid = int(request.POST.get('news'))
				userid = int(request.POST.get('user'))
				# print(newsid)
				# print(userid)
				user = User.objects.get(id=userid)
				print(user)
				news = News.objects.get(id=newsid)
				print(news)
				Rating.objects.all().delete()

				try:
					rating_obj = Rating.objects.filter(user=user,news=news).first()
					form = RatingForm(data=request.POST,instance=rating_obj)
				# rating_s = request.POST.get('rating_s')form.cleaned_data['rating_s'] #request.POST.get(username=form.cleaned_data.get['rate'])
				except Rating.DoesNotExist:
					form = RatingForm(data=request.POST)

				if form.is_valid():
					rating_ = form.save()
					res['rating'] = rating_.id
					res['success']=True	
					return JsonResponse(res)
				
				else:
					res['error']=True
					print(request.POST)
					print(form.errors)
					return JsonResponse(res)

		else:
				res['auth']=False
				return JsonResponse(res)
		
		
		# except:
		# 	print("DoesNotExist")
		# 	return JsonResponse(res)


		# if request.method == "POST":
	# 	if request.user.is_authenticated:
	# 		form = RatingForm(request.POST)
 #            if form.is_valid():
 #            	user = User.objects.get(id=request.user.id)
 #            	cleaned_rating = form.cleaned_data["rating_s"]
 #            	news_id = form.cleaned_data["news_id"]
 #                parent = form.cleaned_data["parent_id"]

 #                print(post_rating(news_id, cleaned_rating, parent, user))

 #            else:
 #            	return render(request, "registration/login.html")

def average_rating(request):
	pid = request.POST.get('news_id', '')
	try:
		obj = News.objects.get(id=1)
	except News.DoesNotExist:
		obj = None
	news_ratings = Rating.objects.filter(news=obj)
	ratings = []
	count ={}
	for r in news_ratings:
		# r.rating_s
		# print(r.rating_s)
		present = True
		print(r.rating_s)
		if r.rating_s not in ratings :
			ratings.append(r.rating_s)
			present= False
		if present:
			count[r.rating_s] +=1
		else:
			count[r.rating_s] =1
	# print(r.rating_s)		
	# sort = ratings.sort(reverse = True)
	# print(sort)
	context = {
	'sort': ratings,
	'count':count,
	}
			# count[r.rating_s]
	# countr_one = 0
	# count_two = 0
	# count_three = 0
	# count_four = 0
	# count_five = 0
	# rating_one = Rating.objects.filter(rating_s="1")
	# if rating_one:
	# 	print("1")
	# 	count_one += 1
	# 	print(count_one)
	# rating_two = Rating.objects.filter(rating_s="2")
	# if rating_two:
	# 	print("2")
	# 	count_two += 1
	# 	print(count_two)
	# rating_three = Rating.objects.filter(rating_s="3")
	# if rating_three:
	# 	print("3")
	# 	count_three += 1
	# 	print(count_three)
	# rating_four = Rating.objects.filter(rating_s="4")
	# if rating_four:
	# 	print("4")
	# 	count_four += 1
	# 	print(count_four)
	# rating_five = Rating.objects.filter(rating_s="5")
	# if rating_five:
	# 	print("5")
	# 	count_five += 1
	# 	print(count_five)
	# sum = Rating.objects.aggregate(Sum('rating_s'))
	# print(sum)
	# user_count = Rating.objects.values("user").count()
	# print(user_count)
	return render(request,"ratings/ratings.html",context)



