from django import forms

from ratings.models import Rating


class RatingForm(forms.ModelForm):
	class Meta:
		rating_s = forms.DecimalField(label='', max_value=5, min_value=0)
		model = Rating
		fields =  ('rating_s','news','user')
		widgets = {
        	'rating_s': forms.TextInput({ 'id':"input-3-ltr-star-md", 
				'class':"kv-ltr-theme-uni-star rating-loading",
				'min':0,
				'max':5,
				'dir':"rtl",
				"required": True,
			}),
            'news': 	forms.HiddenInput({'id':"rating_news"}),
            'user':		forms.HiddenInput({'id':"rating_user"}),
		}
	# news_id = forms.CharField(max_length=150,required= True, widget=forms.TextInput(attrs=
	# 	{
	# 	"hidden": True,
		

	# 	}))
