from django.db import models
from news.models import News
from django.contrib.auth.models import User
# import string
# from ratings.forms import RatingForm

# Create your models here.
class Rating(models.Model):
    class Meta:
        verbose_name_plural = "Ratings"
    
    user = models.ForeignKey(User, on_delete=models.CASCADE ,related_name ='user_rating', verbose_name = "User")
    rating_s = models.DecimalField(verbose_name = "Rating Star", decimal_places=1, max_digits=5)
    news = models.ForeignKey(News, on_delete=models.CASCADE, related_name ='News')
    datetime =  models.DateTimeField(auto_now=True, verbose_name = "Ratings posted on")

    def __str__(self):
        return str(self.rating_s)