from django.db import models

# Create your models here.
class SurveyModel(models.Model):
    question = models.CharField(max_length=120)
    answer = models.CharField(max_length=120)
    email = models.EmailField()