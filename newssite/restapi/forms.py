from django import forms

from .models import model_templates

class RestForm(forms.ModelForm):
    answer = forms.CharField(required=True, widget=forms.Textarea(
								attrs={
								"class": "new-class-name two",
								"id": "my-id-for-textarea",
								"rows": 20,
								'cols': 100
								}))
    class Meta:
        model = model_templates()
        
        fields = [
            
            'answer',
            'views',
            ]