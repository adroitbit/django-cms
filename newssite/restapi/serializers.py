from rest_framework import serializers
from .models import model_templates

class Serializer(serializers.ModelSerializer):
    
    answer = serializers.CharField(max_length=120)
    views = serializers.IntegerField()
    
    class Meta:
        model = model_templates
        fields = ['answer','id','views']
        