from .models import News
from .models import Category
from django.core.paginator import Paginator

from theme.models import SidebarSettings,PlaceCategory
from comments.models import Comment
from django.db.models import Avg
from django.db.models import Sum

from datetime import datetime

from ratings.models import Rating
from sharing.models import  Shares
from analytics.models import AnalyticalData
from easy_thumbnails.files import get_thumbnailer


PER_PAGE_COUNT = 20
def get_category_showcase(cat):
    news = News.objects.filter(category=cat).order_by('-published')[:2]
    return news

def get_home_showcase(lang):
    news = News.objects.filter(category__site=cat).order_by('-published')[:5]


def get_category_child(cat):
    return Category.objects.filter(parent=cat)


def get_all_category_parents(cat):
    parents = []
    while cat.parent is not None:
        parents = [cat.parent] + parents
        parents.insert(0, cat.parent)
        cat = cat.parent
    return parents

def get_category_news_published(cat,count=PER_PAGE_COUNT,page=1):
    news = News.objects.filter(category=cat).order_by('-published')
    paginator = Paginator(news, count)
    pobj = paginator.page(page)
    return pobj.object_list, paginator.page_range

def get_time_ago(date):

    if date is not None:
            time = datetime.now()
            if date.hour == time.hour:
                return  str(time.minute - date.minute) + " minutes ago"
            elif date.day == time.day:
                return  str(time.hour - date.hour) + " hours ago"
            elif date.month == time.month:
                return str(time.day - date.day) + " days ago"
            else:
                return str(date.date())
    return ""
def getNews_data(news):

    has_img = False
    if news.image is not None and not news.image =="":
        has_img = True
    views = AnalyticalData.objects.filter(slug=news.slug).aggregate(Sum('views'))['views__sum']
    rating = Rating.objects.filter(news=news).aggregate(Avg('rating_s'))['rating_s__avg']

    if views is None:
        views= 0
    if rating is None:
        rating= 0

    if has_img:
        return {
        'id'                : news.id,
        'slug'              : news.slug.slug,
        'title'             : news.title,
        'category'          : news.category.all()[0].title,
        'category_slug'     : news.category.all()[0].slug.slug,
        'content'           : news.content,
        'author'            : news.author.get_full_name() or news.author,
        'author_username'   : str(news.author),
        'published'         : get_time_ago(news.published),
        'image'             : news.image.url,
        'thumbnail'         : {
            "featured"          :           get_thumbnailer(news.image)['featured'].url,
            "featured_small"   :           get_thumbnailer(news.image)['featured_small'].url,
            "medium"            :           get_thumbnailer(news.image)['medium'].url,
            "small"             :           get_thumbnailer(news.image)['small'].url,
        },
        'imagesource'       : news.imagesource,
        'sourcedesc'        : news.sourcedesc,
        'has_image'         : has_img,
        'comment_counts'    : Comment.objects.filter(news=news).count(),
        'avg_rating'        : rating,
        'shares'            : Shares.objects.filter(slug=news.slug).count(),
        'views'             : views,
        }
    else:
        return {
        'id'                : news.id,
        'slug'              : news.slug.slug,
        'title'             : news.title,
        'category'          : news.category.all()[0].title,
        'category_slug'     : news.category.all()[0].slug.slug,
        'content'           : news.content,
        'author'            : news.author.get_full_name() or news.author,
        'author_username'   : str(news.author),
        'published'         : get_time_ago(news.published),
        'has_image'         : has_img,
        'comment_counts'    : Comment.objects.filter(news=news).count(),
        'avg_rating'        : rating,
        'shares'            : Shares.objects.filter(slug=news.slug).count(),
        'views'             : views,
        }

def get_latest_category(site):
    category = Category.objects.filter(site=site)
    category_list = list(category)
    return category_list

def get_sidebar(site):
    sidebar = SidebarSettings.objects.filter(site=site).order_by('index')
    # print(sidebar)
    return sidebar

def get_all_categoryplace(lang):
    category_place = PlaceCategory.objects.filter(shown=True, cat__site=lang).exclude(placeholder__title__exact='cat_news_half').order_by('sindex', 'placeholder')
    category_place_list = list(category_place)
    # print(category_place_list)
    return category_place_list

def get_half_categoryplace(lang):
    category_place = PlaceCategory.objects.filter(shown=True, placeholder__title='cat_news_half', cat__site=lang).order_by('sindex', 'placeholder')
    category_place_list = list(category_place)
    # print(category_place_list)
    return category_place_list

def check_normal(lang):
    place_normal = PlaceCategory.objects.filter(shown=True, cat__site =lang, placeholder__title="cat_news_normal")
    return place_normal


def get_slide(lang):
    place_normal = PlaceCategory.objects.filter(shown=True, cat__site =lang, placeholder__title="cat_news_slide")
    return place_normal

def get_half(lang):
    place_normal = PlaceCategory.objects.filter(shown=True, cat__site =lang, placeholder__title="cat_news_half")
    return place_normal
