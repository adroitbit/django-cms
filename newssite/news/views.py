from django.shortcuts import render, get_object_or_404

from .models import SiteSlug, Category, Tag, News, Page
# Create your views here.
from django.http import Http404
from usermgt.functions import is_action_permitted
from theme.functions import print_bread, get_child_cats
from usermgt.models import NTUser
from django.contrib.auth.models import User

from news.functions import get_sidebar,get_all_categoryplace,check_normal,get_slide,get_half, get_half_categoryplace

from .functions import getNews_data
from django.core.paginator import Paginator

from theme.models import SiteSettings,SidebarSettings,PlaceCategory

from comments.forms import CommentForm
from comments.functions import get_parent_comments

from ratings.forms import RatingForm
from ratings.views import ratings_view

from comments.const import COMMENTS_PER_REQUEST

from ratings.models import Rating
from analytics.functions import update_analytics, get_most_viewed
from sharing.functions import get_most_shares
from seo.functions import getSiteSEOtags, getSlugSEOtags
from sharing.views import monitor_shares

def home_view(request):
    category = Category.objects.all()
    template = 'pages/home.html'
    context = {'categories':category}
    return render(request, template,context)


def page_view(request, slug, lang='en'):
    page = Page.objects.filter(slug=slug, site=lang).first()
    site_setting = SiteSettings.objects.get(site=lang)
    template = 'site-pages/page-post.html'
    context = {
        'page':page,
        'preview': False,
        'site':site_setting,
    }
    if page.status == 'published':
        return render(request, template,context)
    else:
        if request.user.is_authenticated:
            user = User.objects.get(id=request.user.id)
            try:
                ntuser = NTUser.objects.get(user=user)
                if page.status is not 'published' and is_action_permitted('preview_page', ntuser.user_type):
                    context['preview']=True
                    return render(request, template, context)
                else:
                    return render(request, '404.html' ,context)
            except  NTUser.DoesNotExist:
                print("NT user not found")
                return render(request, '404.html' ,context)

        else:
            return render(request, '404.html' ,context)

def handle_news(request, news,cat_obj,related_news):

    # template = 'site-pages/news-post.html'
    lang = news.get_news_site()
    site_setting = SiteSettings.objects.get(site=lang)
    # print(site_setting)
    template = 'site-pages/news-post.html'
    comments , comment_count , _ = get_parent_comments(news, count=COMMENTS_PER_REQUEST)
    ratings = ratings_view(request)
    related = []
    for n in related_news:
        related.append(getNews_data(n))
    context = {
        'news'          : getNews_data(news),
        'preview'       : False,
        'site'          : site_setting,
        'comment_form'  : CommentForm,
        'rating_form'   : RatingForm,
        'ratings'       : ratings,
        'comments'      : comments,
        'logedin'       : request.user.is_authenticated,
        'comment_count' : comment_count,
        'comment_more'  : comment_count- len(comments)>0,
        'related_news'  : related,
        'cat_obj'       : cat_obj,
        'breadcrumb'    : print_bread(cat_obj[0]),
        'sidebar'       : get_sidebar(cat_obj[0].site),
        'meta'          : "",
    }
    context['meta'] =  getSiteSEOtags(lang) + getSlugSEOtags(news.slug,news)
    share =  request.GET.get('share','')
    
    if not share == "":
        monitor_shares(request)
    # print(news.status)
    if request.user.is_authenticated:
            user = User.objects.get(id=request.user.id)
            comment_form = CommentForm(initial={'news': news,'user': user,"parent":None})
            rating_obj = Rating.objects.filter(user=user,news= news).first()
            # rating_obj = None
            if rating_obj is not None:
                rating_form =RatingForm(initial={'news': news,'user': user, 'rating_s':rating_obj.rating_s},instance=rating_obj)
                print("rating exist")
            else:
                # rating_s = request.POST.get('rating_s')form.cleaned_data['rating_s'] #request.POST.get(username=form.cleaned_data.get['rate'])
                # print("rating does not exist")
                rating_form = RatingForm(initial={'news': news,'user': user})
            context['comment_form']=comment_form
            context['rating_form']=rating_form

            try:
                ntuser = NTUser.objects.get(user=user)
                # comment_form = CommentForm()
                # context['comment_form']=comment_form
                # if request.method == 'POST':
                #     pass
                # print(news.status)
                if not news.status == 'published' and is_action_permitted('preview_post', ntuser.user_type):
                    context['preview']=True
                    comment_form = CommentForm(initial={'news': news,'user': user,"parent":None})
                    context['comment_form']=comment_form
                    return render(request, template, context)

                elif news.status == "published":
                    # print("published")
                    # print(news)
                    # context['comments'], c_range = get_parent_comments(news)
                    # print(context['comments'])
                    # rating_form = RatingForm(initial={'news': news,'user': user})
                    # context['comment_form']=comment_form
                    # context['rating_form']=rating_form


                    # print(comment_form)
                    return render(request, template,context)
                else:
                    return render(request, '404.html' ,context)
            except  NTUser.DoesNotExist:
                if news.status == 'published':
                    return render(request, template,context)
                else:
                    return render(request, '404.html' ,context)


    elif news.status == 'published':
        return render(request, template,context)
    else:
        return render(request, '404.html' ,context)

    # return render(request, template,context)

def slug_view(request, slug, lang='en'):
    try:
        slugObj = SiteSlug.objects.get(slug=slug)
        print(slug)
        print("slug view")
        if slugObj.model == 'cat':
            update_analytics(request,slugObj)
            return category_template(request, slugObj)

            # cat = Category.objects.get(slug=slugObj)
            # template = 'pages/category.html'
            # context = {'category':cat}
            # return render(request, template,context)
        elif slugObj.model == 'tag':
            tag = Tag.objects.get(slug=slugObj)
            template = 'pages/tag.html'
            context = {'tag':tag}
            update_analytics(request,slugObj)
            return render(request, template,context)

        # elif slugObj.model == 'page':
        #     page = Page.objects.get(slug=slugObj)
        #     template = 'pages/page.html'
        #     context = {'page':page}
        #     return render(request, template,context)

        elif slugObj.model == 'news':
            # print("got slug {}".format(slug))
            update_analytics(request,slugObj)
            news = News.objects.get(slug=slugObj)
            # print(news.category.all())
            cat_obj = news.category.all()
            # print(cat_obj)
            related_news = News.objects.filter(status='published',category__in=cat_obj).exclude(id__in=[news.id])[:3]
            return handle_news(request, news,cat_obj,related_news)

    except SiteSlug.DoesNotExist:
        template = '404.html'
        context = {
            'site':SiteSettings.objects.get(site=lang),
        }
        return render(request, template,context)

    # slugObj = get_object_or_404(SiteSlug, slug)
def category_template(request, siteslug):
    if request.method == "GET":
        sort = request.GET.get('sort','')
        possible_val = ['most_viewed','most_viewed_week','most_shared','most_shared_week']
        # siteslug = SiteSlug.objects.get(slug=slug)
        # print(siteslug)
        cat_obj =  Category.objects.filter(slug=siteslug).first()
        lang = cat_obj.site
        print(cat_obj)
        cat_news = News.objects.filter(status='published',category=cat_obj).exclude(image__isnull=True).exclude(image__exact='').order_by('-published')[:2]

        # print(news_first)
        ids = []
        showcase_news = []
        news = []
        for s in cat_news:
            data = getNews_data(s)
            ids.append(s.id)
            # print(data)
            showcase_news.append(data)
        # print(news_obj)
        if sort == 'most_viewed':
            news_obj = get_most_viewed(cat_obj)
        elif  sort == 'most_viewed_week':
            news_obj = get_most_viewed(cat_obj, sevendaysago=True)
        elif  sort == 'most_shared':
            news_obj = get_most_shares(cat_obj)
        elif  sort == 'most_shared_week':
            news_obj = get_most_shares(cat_obj, sevendaysago=True)
        else:
            news_obj = News.objects.filter(status='published',category=cat_obj).exclude(id__in=ids).order_by('-published')
            
        # print(news_obj)    
        site_setting = SiteSettings.objects.get(site=cat_obj.site)
        paginator = Paginator(news_obj,1)
        page = int(request.GET.get('page',1))
        obj_page = paginator.get_page(page)
        # print(cat_obj.slug.slug)
        obj_sidebar = get_sidebar(cat_obj.site)

        for n in obj_page.object_list:
            data = getNews_data(n)
            news.append(data)
            # print(n.views)
            # print(n.shares)
        context = {
        # 'showcase': get_category_showcase(slug),
        'siteslug':siteslug,
        'category': cat_obj,
        'news': news,
        'first': showcase_news,
        'site':site_setting,
        'page':obj_page,
        'sidebar': obj_sidebar,
        'last_page': paginator.num_pages,
        'current_page' : page,
        'breadcrumb': print_bread(cat_obj),
        'child_links': get_child_cats(cat_obj),
        'meta'          :  getSiteSEOtags(lang) + getSlugSEOtags(siteslug,cat_obj),
        'sort'          : sort,
        }
    return render(request, "site-pages/category-page.html",context)

def home_template(request, lang='en'):
    if request.method == "GET":
        # site = request.GET.get('site','')
        site_setting = SiteSettings.objects.get(site=lang)
        news = None
        news_normal = None
        news_slide = None
        news_first_half = None
        news_second_half = None
        sec_size = {
            'cat_news_normal'   : 6,
            'cat_news_half'     : 5,
            'cat_news_slide'    : 3,
            'cat_news_special'  : 9,
        }
        # news_cat = Category.objects.filter(site=site)
        obj_catplace = list(get_all_categoryplace(lang))
        obj_half_catplace =  list(get_half_categoryplace(lang))
        showcase = News.objects.filter(status='published', category__site=lang).exclude(image__isnull=True).exclude(image__exact='').order_by('-published')[:5]
        showcase_news = []
        sections = []
        half_sections = []
        ids = []
        for s in showcase:
            data = getNews_data(s)
            ids.append(s.id)
            # print(data)
            showcase_news.append(data)
        if not len(obj_half_catplace)  == 0 and not len(obj_half_catplace) % 2 == 0:
            obj = obj_half_catplace[len(obj_half_catplace)-1]
            obj.placeholder.title = 'cat_news_normal'
            obj_catplace.append(obj)
            del obj_half_catplace[len(obj_half_catplace)-1]

        for section in obj_catplace:
            # c.cat
            n = News.objects.filter(status='published', category=section.cat).exclude(id__in=ids).order_by('-published')[:sec_size[section.placeholder.title]]
            news = []
            for posts  in n:
                data = getNews_data(posts)
                # print(data)
                news.append(data)
            if section.placeholder.title == 'cat_news_special':
                s = {
                'category'      : section.cat.title,
                'category_slug' : section.cat.slug.slug,
                'placeholder'   : section.placeholder.title,
                'first_news'    : news[0],
                'second_col'    : news[1:4],
                'third_col'    : news[5:],
                }
            else:
                s = {
                'category'      : section.cat.title,
                'category_slug' : section.cat.slug.slug,
                'placeholder'   : section.placeholder.title,
                'news'          : news,
                }
            sections.append(s)
            first = True
            temp = None
            for section in obj_half_catplace:
                # c.cat
                n = News.objects.filter(status='published', category=section.cat).exclude(id__in=ids).order_by('-published')[:sec_size[section.placeholder.title]]
                news = []
                for posts  in n:
                    data = getNews_data(posts)
                    # print(data)
                    news.append(data)
                s = {
                    'category'      : section.cat.title,
                    'category_slug' : section.cat.slug.slug,
                    'placeholder'   : section.placeholder.title,
                    'news'          : news,
                }
                if first and temp is None:
                    temp = s
                    first = False
                else:
                    half_sections.append([temp , s])
                    temp = None
                    first= True

            # print(c.cat)

            # print(news)


        # cat_places_normal = check_normal(lang)
        # for c in cat_places_normal:
        #     # c.cat
        #     # print(c.cat)
        #     news_normal = News.objects.filter(category=c.cat).order_by('-published')[:6]

        # cat_places_slide = get_slide(lang)
        # for c in cat_places_slide:
        #     # c.cat
        #     # print(c.cat)
        #     news_slide = News.objects.filter(category=c.cat).order_by('-published')[:3]

        # cat_places_half = get_half(lang)
        # for c in cat_places_half:
        #     # c.cat
        #     # print(c.cat)
        #     news_first_half = News.objects.filter(category=c.cat).order_by('-published')[:5]
        #     news_second_half = News.objects.filter(category=c.cat).order_by('-published')[5:]
        context = {
        'site'          : site_setting,
        'showcase_news' : showcase_news,
        'sections'      : sections,
        'half_sections' : half_sections,
        'meta'          :  getSiteSEOtags(lang)

        # 'normal': news_normal,
        # 'slide': news_slide,
        # 'half_f': news_first_half,
        # 'half_s': news_second_half,
        }
    return render(request, "site-pages/home-page.html",context)
