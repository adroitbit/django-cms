from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from django.contrib import admin
# from ckeditor.fields import RichTextField
# from ckeditor_uploader.fields import RichTextUploadingField

import string
import random

# from django.core.urlresolvers import reverse
from django.urls import reverse
from datetime import datetime

from django.core import validators
from django.db.models.signals import pre_save

# from filer.fields.image import FilerImageField
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill

from django.db.models.signals import m2m_changed
# import datetime


DEFINED_SITES = (
        ('en','English'),
        ('np','Nepali'),
        ('ch','Chineese'),
    )

DEFINED_SITES_DICT = {
        'en':'English',
        'np':'Nepali',
        'ch':'Chineese',
    }

DEFINED_POST_STATUS = (
        ('draft','Draft'),
        ('published','Published'),
        ('under_review','Under Review'),
        ('awaiting_changes','Awaiting Changes'),
        ('review_final','Finished Review'),
        ('final','Ready for Publishing'),
        ('rejected','Rejected'),
        ('trash','Trash'),
        )

DEFINED_POST_STATUS_DICT = {
    'draft'             : 'Draft',
    'published'         : 'Published',
    'under_review'      : 'Under Review',
    'awaiting_changes'  : 'Awaiting Changes',
    'review_final'      : 'Finished Review',
    'rejected'          : 'Rejected',
    'trash'             : 'Trash',
    'final'             : 'Ready for Publishing',
}

SLUG_OPTIONS = {
    ('cat','Category'),
    ('tag','Tag'),
    ('news','News'),
    ('page','Page'),
}

# Create your models here.

class SiteSlug(models.Model):
    class Meta:
        verbose_name_plural = "Site Slug"
        verbose_name = "Site Slug"
    slug = models.SlugField(unique=True)
    model = models.CharField(max_length=4, choices =SLUG_OPTIONS, default = 'news')
    item_id = models.PositiveIntegerField(default=0)

    def get_absolute_url(self):
        return reverse("slug_page", args = [self.slug])

    def get_random_string(self, N=10):
        return ''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits) for _ in range(N))

    def _get_unique_slug(self, title):
        slug = slugify(title)
        # if slug is '':
        #     slug = self.get_random_string(20)
        unique_slug = slug
        # print(slug)
        num = 1
        while SiteSlug.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(slug, num)
            num += 1
        return unique_slug

    def save(self,title ,*args, **kwargs):
        # print(slugify(title))
        # print(self.slug)
        if slugify(title) == "":
            self.slug = "{}--{}".format(self.model, self.get_random_string(20))

        super(SiteSlug, self).save(*args, **kwargs)

    def __str__(self):
        return self.slug

class Category(models.Model):
    class Meta:
        verbose_name_plural = "Categories"
    site_choices = DEFINED_SITES
    title= models.CharField(max_length=50,unique=True)
    # slug = models.SlugField(unique=True)
    slug= models.ForeignKey(SiteSlug, on_delete=models.CASCADE ,related_name ='category_slug') #models.SlugField(unique=True)
    # seo_title = models.CharField(max_length=250, verbose_name = "SEO Title")
    # seo_description = models.CharField(max_length=160, verbose_name = "SEO Description")
    # seo_keywords = models.CharField(max_length=160, verbose_name = "SEO Keywords")
    parent = models.ForeignKey('self', verbose_name = "Parent category", blank=True, null=True, on_delete=models.CASCADE,related_name='child_of')
    site = models.CharField(max_length=4, choices =site_choices, default = 'en')
    is_shown = models.BooleanField(verbose_name = "Show on Site", default=True)
    news_count =  models.PositiveIntegerField(default=0,verbose_name = "News Counts")
    description = models.TextField(blank=True, null=True,)
    def save(self, *args, **kwargs):
        if self.pk is None:
            slugObj = SiteSlug()
            uslug = slugObj._get_unique_slug(title = self.title)
            slugObj.slug=uslug
            slugObj.model = 'cat'
            slugObj.save(title = self.title)
            self.slug = slugObj
        if slugify(self.title) not in self.slug.slug and (not slugify(self.title) == ""):
            self.slug.slug = SiteSlug()._get_unique_slug(title = self.title)
            self.slug.save(title = self.title)
        super(Category, self).save(*args, **kwargs)

    def __str__(self):
        return self.title +" ("+self.site+")"

class Tag(models.Model):
    class Meta:
        verbose_name_plural = "Tags"
    site_choices = DEFINED_SITES
    title= models.CharField(max_length=50,unique=True)
    # slug= models.SlugField(unique=True)
    slug= models.ForeignKey(SiteSlug, on_delete=models.CASCADE ,related_name ='tag_slug')

    # seo_title = models.CharField(max_length=250, verbose_name = "SEO Title")
    # seo_description = models.CharField(max_length=160, verbose_name = "SEO Description")
    # seo_keywords = models.CharField(max_length=160, verbose_name = "SEO Keywords")
    site = models.CharField(max_length=4, choices =site_choices, default = 'en')
    news_count =  models.PositiveIntegerField(default=0,verbose_name = "News Counts")
    description = models.TextField(blank=True, null=True)
    def save(self, *args, **kwargs):
        if self.pk is None:
            slugObj = SiteSlug()
            uslug = slugObj._get_unique_slug(title = self.title)
            slugObj.slug=uslug
            slugObj.model = 'tag'
            slugObj.save(title = self.title)
            self.slug = slugObj
        if slugify(self.title) not in self.slug.slug and (not slugify(self.title) == ""):
            self.slug.slug = SiteSlug()._get_unique_slug(title = self.title)
            self.slug.save(title = self.title)

        super(Tag, self).save(*args, **kwargs)

    def __str__(self):
        return self.title +" ("+self.site+")"



class Media(models.Model):
    class Meta:
        verbose_name_plural = "Attachments"
    type_choices= (
        ('youtube','Youtube Video'),
        ('audio','Audio'),
        ('soundcloud','Soundcloud Audio'),
        )
    title= models.CharField(max_length=100)
    media_type = models.CharField(max_length=10, choices =type_choices, default = 'youtube')
    link = models.CharField(max_length=1024, unique=True)
    def __str__(self):
        return self.title

# pre_save.connect(validate_category_signal_handler,
#   sender=Category,
#   dispatch_uid='validate_category_title')


# class AbstractNews(models.Model):
#     class Meta:
#         abstract = True
#         # verbose_name_plural = "News"



class News(models.Model):
    class Meta:
        verbose_name_plural = "News"

    title = models.CharField(max_length=250,unique=True)
    category = models.ManyToManyField(Category, related_name ='news_category', verbose_name = "News Category")
    author = models.ForeignKey(User, on_delete=models.CASCADE ,related_name ='news_post', verbose_name = "Author")
    slug = models.ForeignKey(SiteSlug, on_delete=models.CASCADE ,related_name ='news_slug')
    content = models.TextField()
    published = models.DateTimeField(blank=True, null=True, verbose_name = "Published Date")
    updated  = models.DateTimeField(auto_now=True, verbose_name = "Last updated")
    status = models.CharField(max_length=25, choices =DEFINED_POST_STATUS, default = 'draft', verbose_name = "Status")
    tag = models.ManyToManyField(Tag,blank=True, related_name ='news_tag', verbose_name = "News Tags")
    locked = models.BooleanField(verbose_name = "Lock Modifications", default=False)
    contributers = models.ManyToManyField(User,blank=True, related_name ='news_contributers', verbose_name = "Other Contributers")
    ### modify
    image = models.ImageField(upload_to='featured/%Y/%m/%d/',blank=True,null=True,  default = None, verbose_name = "Featured Image")
    imagesource = models.URLField(max_length=256,blank=True, default=None, null=True, verbose_name = "Featured Image Source")
    sourcedesc = models.CharField(max_length=100,blank=True, default=None, null=True, verbose_name = "Image Source Description")
    attachment = models.ForeignKey(Media, blank=True, null=True, on_delete=models.CASCADE,related_name ='other_attachment', verbose_name = "Featured content")

    def all_categories(self):
        return ",".join([p.__str__() for p in self.category.all()])

    all_categories.short_description = "Categories"

    def site_categories(self,site=None):
        if site is None:
            cat_ = self.category.all()[0]
            site = cat_.site
        return [p.__str__() for p in Category.objects.filter(site=site)]

    def get_news_site(self):
        cat_ = self.category.all()[0]
        return cat_.site


    def all_tags(self):
        return ",".join([p.__str__() for p in self.tag.all()])

    all_tags.short_description = "Tags"

    def save(self , history=False, *args, **kwargs):
        self.updated = timezone.now
        if  self.pk is not None and self.slug.item_id == 0:
            self.slug.item_id = self.id
            self.slug.save(title=self.title)

        if self.pk is None:
            slugObj = SiteSlug()
            uslug = slugObj._get_unique_slug(title = self.title)
            slugObj.slug=uslug
            slugObj.model = 'news'
            slugObj.save(title = self.title)
            self.slug = slugObj
            self.created = timezone.now
            # self.author = User.objects.get(id=request.user.id)

        if slugify(self.title) not in self.slug.slug and (not slugify(self.title) == ""):
            self.slug.slug = SiteSlug()._get_unique_slug(title = self.title)
            self.slug.save(title = self.title)

        news = super(News, self).save(*args, **kwargs)
        if history:
            NewsHistory.objects.create(
                post=news,
                title=news.title,
                content=news.content,
                author=news.author,
                # image=news.image,
                # imagesource=news.imagesource,
                # sourcedesc=news.sourcedesc,
            )

    # def update_news_status(self, status='draft', *args, **kwargs):
    #     self.status = status
    #     if (self.status == 'published') and (self.published is None):
    #         self.published = timezone.now
    #     super(News, self).save(force_update=True,*args, **kwargs)
    def get_time_ago(self):
        if self.published is not None:
            time = datetime.now()
            if self.published.hour == time.hour:
                return "Published "+ str(time.minute - self.published.minute) + " minutes ago"
            elif self.published.day == time.day:
                return "Published "+ str(time.hour - self.published.hour) + " hours ago"
            elif self.published.month == time.month:
                return "Published "+ str(time.day - self.published.day) + " days ago"
            else:
                return "Published on "+ str(self.published.month.date())
        else:
            return "Unpublished Article"

    def __str__(self):
        return self.title

class NewsHistory(models.Model):
    title = models.CharField(max_length=250)
    content = models.TextField()
    created = models.DateTimeField(auto_now_add=True, verbose_name = "Created Date")
    post = models.ForeignKey(News, on_delete=models.CASCADE,related_name ='news_history', verbose_name = "News post")
    author = models.ForeignKey(User, on_delete=models.CASCADE ,related_name ='history_post', verbose_name = "Author")

    class Meta:
        ordering = ['-pk']


def category_changed(sender, instance, action,  **kwargs):
    # Do something
    # print()
    if action =='post_add':
        for cat in list(instance.category.all()):
            # print(cat)
            categ = Category.objects.get(id=cat.id)
            total = News.objects.filter(category = categ).count()
            categ.news_count = total
            categ.save()

m2m_changed.connect(category_changed, sender=News.category.through)

def tags_changed(sender, instance, action,  **kwargs):
    # Do something
    # print()
    if action =='post_add':
        for tag in list(instance.tag.all()):
            tagobj = Tag.objects.get(id=tag.id)
            total = News.objects.filter(tag = tagobj).count()
            tagobj.news_count = total
            tagobj.save()

m2m_changed.connect(tags_changed, sender=News.tag.through)


class Page(models.Model):
    menu_choices = (
        ('top','Top Menu'),
        ('site','Site Links'),
        ('other','Other'),
        )
    class Meta:
        verbose_name_plural = "Pages"
        unique_together = ('site', 'title',)
    title = models.CharField(max_length=250)
    slug = models.SlugField()
    site = models.CharField(max_length=4, choices =DEFINED_SITES, default = 'en')
    # slug= models.ForeignKey(SiteSlug, on_delete=models.CASCADE ,related_name ='page_slug')
    content = models.TextField()
    # seo_title = models.CharField(max_length=250, verbose_name = "SEO Title")
    # seo_description = models.CharField(max_length=160, verbose_name = "SEO Description")
    # seo_keywords = models.CharField(max_length=160, verbose_name = "SEO Keywords")
    published = models.DateTimeField(blank=True, null=True, verbose_name = "Published Date")
    created = models.DateTimeField(auto_now_add=True, verbose_name = "Created Date")
    updated  = models.DateTimeField(auto_now=True, verbose_name = "Last updated")
    author = models.ForeignKey(User, on_delete=models.CASCADE ,related_name ='page_post', verbose_name = "Author")
    contributers = models.ManyToManyField(User,blank=True, related_name ='page_contributers', verbose_name = "Other Contributers")
    status = models.CharField(max_length=7, choices =DEFINED_POST_STATUS, default = 'draft', verbose_name = "Status")
    menu = models.CharField(max_length=5, choices =menu_choices, default = 'top')

    has_sidebar = models.BooleanField(verbose_name = "Show sidebar", default=True)
    def save(self, *args, **kwargs):
        self.updated = timezone.now
        if self.pk is None:
            self.slug = slugify(self.title)
            self.created = timezone.now
        super(Page, self).save(*args, **kwargs)

    def __str__(self):
        return self.title
