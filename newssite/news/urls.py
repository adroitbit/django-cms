from django.conf.urls import url

from . import views as news_views
from django.urls import path,include
from subscription.views import model_view
from polls.views import model_poll_view,model_poll_dyanmic_view
from survey.views import model_survey_view
from rest_framework.urlpatterns import format_suffix_patterns
from restapi import views
from comments.views import model_comment_view, model_get_comment, model_get_child_comment
from ratings.views import ratings_view,average_rating
from sharing.views import share_news
from AdMgmt.views import addmodel_view,click_view
from restapimobile.views import RestView,SortView,SortByPopularity,GetCategoryView,ApiKeyView,CheckApi,TagView,ApiView,NewsIdViews,NewsUpdateViews
from news.views import category_template,home_template
from analytics.views import analytics_view
# from seo_app.views import seo_site_view,site_description_view

from usermgt.views import register, user_login, user_logout ,account, add_post ,apply_as, edit_post, publish_post
from usermgt.dashboard import list_posts, assign_posts, review_posts, preview_post_changes
# from django.contrib.auth.views import login
from django.conf import settings
from django.conf.urls.static import static
from theme.news_import import  get_news

urlpatterns = [
    # url(r"^$", views.home_view, name ="home_page"),
    path('subscription/', model_view),
    path('pollanswer/', model_poll_view),
    path('polldynamic/<int:pid>/', model_poll_dyanmic_view),
    path('survey/', model_survey_view),
    path('account/register/', register),
    path('account/login/',user_login),
    path('account/logout/',user_logout),
    path('account/', account),
    path('account/<str:site>/', account),
    path('account/post/add/<str:site>/', add_post),
    path('account/post/list/', list_posts),
    path('account/post/list/<str:site>/', list_posts),
    path('account/post/assign/<str:slug>/', assign_posts),
    path('account/post/review/<str:slug>/', review_posts),
    path('account/post/edit/<str:slug>/', edit_post),
    path('account/post/publish/<int:postid>/', publish_post),
    path('preview/post/<int:hist_id>/', preview_post_changes),
    path('account/apply/<str:role>/', apply_as),
    # path('rest/',views.List.as_view()),
    path('rest/<int:pid>/',views.ViewList.as_view()),
    path('user/',views.restapi_view),
    path('addcomment/',model_comment_view),
    path('getcomment/',model_get_comment),
    path('getchid_comments/',model_get_child_comment),
    # path('getchilds/',child_view.as_view()),
    path('add_ratings/',ratings_view),
    # path('average/',average_rating),
    path('news_share/',share_news),
    path('news_import/',get_news),
    # path('shared/',referer_view),
    # path('shared/url/',redirect),
    path('advertise/',addmodel_view),
    path('clicks/',click_view),
    path('api/language/getcategories/<str:site>/',RestView.as_view()),
    path('sortbydate/',SortView.as_view()),
    path('sortbypopularity/',SortByPopularity.as_view()),
    path('api/language/gettags/<str:site>/',TagView.as_view()),
    path('api/language/getnews/',GetCategoryView.as_view()),
    path('apikeymodel/',ApiView.as_view()),
    path('addkey/',ApiKeyView),
    path('api/language/getnews/<int:pid>/',NewsIdViews.as_view()),
    path('api/language/news_update/<int:pid>/',NewsUpdateViews.as_view()),

    path('category/',category_template),
    path('',home_template),
    # path('analytics/',analytics_view),
    # path('seo/',seo_site_view),
    # path('seodescription/<str:locale>/',site_description_view),
    # path('analyticspd/',analyticspd),

    path('',include('user_auth.urls')),
    path('accounts/',include('django.contrib.auth.urls')),
    path('p/<str:slug>/', news_views.page_view),
    path('<str:lang>/p/<str:slug>/', news_views.page_view),
    path('<str:slug>/', news_views.slug_view, name ="slug_page"),
    path('<str:lang>/<str:slug>/', news_views.slug_view, name ="slug_page"),

    # static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT),

    # url(r"^(?P<slug>[\w]+)/$", news_views.slug_view, name ="slug_page"),
    # path('admin/', admin.site.urls),
    # re_path(r'^files/', include('filer.urls')),


]


urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'html']) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
