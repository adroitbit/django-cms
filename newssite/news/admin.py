

from django.contrib import admin

# Register your models here.
from .models import Category
# from .models import Attachment
from .models import News
from .models import Tag
from .models import DEFINED_SITES, DEFINED_POST_STATUS
from django.utils import timezone

from .models import Media

from .models import Page
from django_summernote.admin import SummernoteModelAdmin


class TagFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = 'Site language'
    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'site'
    def queryset(self, request, queryset):
        if self.value():
           return queryset.filter(site=self.value())
        else:
            return queryset
            
    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return DEFINED_SITES


class TagAdmin(admin.ModelAdmin):
    exclude = ("slug", 'news_count')
    list_display = ('title', 'site', 'slug', 'news_count')
    list_filter = (TagFilter,)


class CategoryFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = 'Site language'
    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'site'
    def queryset(self, request, queryset):
        if self.value():
           return queryset.filter(site=self.value())
        else:
            return queryset
            
    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return DEFINED_SITES


class CategoryAdmin(admin.ModelAdmin):
    exclude = ("slug", 'news_count' )
    list_display = ('title', 'site', 'slug', 'news_count')
    list_filter = (CategoryFilter,)

class NewsStatusFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = 'News Status'
    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'status'
    def queryset(self, request, queryset):
        if self.value():
           return queryset.filter(status=self.value())
        else:
            return queryset
            
    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return (
        ('draft','Draft'),
        ('published','Published'),
        ('under_review','Under Review'),
        ('rejected','Rejected'),
        )

# def publish_selected_news(modeladmin, request, queryset):
#     queryset.update(status='published')


class NewsAdmin(SummernoteModelAdmin):
    summernote_fields = ('content')
    date_hierarchy = 'updated'
    exclude = ("slug",'status','published','created','updated','author')
    list_display = ('title',"slug",'all_categories', 'all_tags', 'status','updated','author','published')
    list_filter = (NewsStatusFilter, 'updated','published')
    search_fields = ['title']
    actions = ['publish_selected_news','unpublish_selected_news']
    
    def publish_selected_news(self, request, queryset):
        rows_updated=0
        for q in queryset:
            rows_updated=1+rows_updated
            q.published = timezone.now()
            q.status='published'
            q.save()
            
        # rows_updated = queryset.update(status=True)
        if rows_updated == 1:
            message_bit = "1 News was"
        else:
            message_bit = "%s News were" % rows_updated
        self.message_user(request, "%s successfully marked as published." % message_bit)
        
    publish_selected_news.short_description = "Publish selected News"
    
    def unpublish_selected_news(self, request, queryset):
        rows_updated=0
        for q in queryset:
            rows_updated=1+rows_updated
            q.published=None
            q.status='under_review'
            q.save()
        
        if rows_updated == 1:
            message_bit = "1 News was"
        else:
            message_bit = "%s News were" % rows_updated
        self.message_user(request, "%s successfully marked as published." % message_bit)
        
    unpublish_selected_news.short_description = "Submit selected News for Review"
    
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.author = request.user
        super().save_model(request, obj, form, change)
    #list_filter = (CategoryFilter,)


    
admin.site.register(Category,CategoryAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(News, NewsAdmin)


admin.site.register(Media)

class PageStatusFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = 'Page Status'
    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'status'
    def queryset(self, request, queryset):
        if self.value():
           return queryset.filter(status=self.value())
        else:
            return queryset
            
    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return (
        ('draft','Draft'),
        ('published','Published'),
        ('under_review','Under Review'),
        ('rejected','Rejected'),
        )

class PageAdmin(admin.ModelAdmin):
    exclude = ("slug",'status','published','created','updated','author')
    list_display = ( 'title', 'site',"slug", 'status','updated','author','published')
    list_filter = (PageStatusFilter, 'updated','published')
    actions = ['publish_selected_page','unpublish_selected_page']
    
    def publish_selected_page(self, request, queryset):
        rows_updated=0
        for q in queryset:
            rows_updated=1+rows_updated
            q.published = timezone.now()
            q.status='published'
            q.save()
            
        # rows_updated = queryset.update(status=True)
        if rows_updated == 1:
            message_bit = "1 Page was"
        else:
            message_bit = "%s Page were" % rows_updated
        self.message_user(request, "%s successfully marked as published." % message_bit)
        
    publish_selected_page.short_description = "Publish selected Page"
    
    def unpublish_selected_page(self, request, queryset):
        rows_updated=0
        for q in queryset:
            rows_updated=1+rows_updated
            q.published=None
            q.status='under_review'
            q.save()
        
        if rows_updated == 1:
            message_bit = "1 Page was"
        else:
            message_bit = "%s Page were" % rows_updated
        self.message_user(request, "%s successfully marked as published." % message_bit)
        
    unpublish_selected_page.short_description = "Submit selected Page for Review"
    
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.author = request.user
        super().save_model(request, obj, form, change)
        
admin.site.register(Page, PageAdmin)

