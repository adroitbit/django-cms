from django.shortcuts import render,redirect
from .forms import AssignPostsForm, PostHistoryForm, ReviewPostsForm
from .models import NTUser
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse

from django.urls import reverse
from django.contrib.auth.decorators import login_required
# from django.utils import timezone
# from datetime import datetime
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.utils.html import strip_tags

from .functions import get_ntuser_context ,get_slug_news , is_action_permitted, check_lang_error, get_NTUser_sites, get_user_can, check_user_assigned_for_review
from .functions import perform_news_list_action
from .models import RoleObj

from django.core import serializers
import json

from django.http import JsonResponse

from usermgt.models import Activity
from news.models import News, NewsHistory
from theme.models import SiteSettings

from django.core.paginator import Paginator

from news.models import DEFINED_SITES

@login_required(login_url='{}{}'.format(settings.SITE_URL , settings.LOGIN_URL))
def list_posts(request,site='en'):
    if request.user.is_authenticated:
        user = User.objects.get(id=request.user.id)
        try:
            ntuser = NTUser.objects.get(user=user)
            submitted = False
            # site = request.GET.get('site') or request.POST.get('site')
            # if site not in DEFINED_SITES:
            #     site = 'en'
            allsite = get_postlist_sites(ntuser)

            if len(allsite) >0 and site not in allsite:
                return redirect(settings.DASHBOARD_URLS.post_list + allsite[0])

            context = get_ntuser_context(ntuser, site)
            context['action']= "List"
            # print(ntuser.user_type)
            # print(ntuser.user.id)
            # print(slug)
            # sites = get_NTUser_sites(ntuser)
            if ntuser.is_rejected:
                context['error'] = "rejected"
                context['message'] = ntuser.rejection_msg
                return render(request, 'dashboard/list-posts.html',context)

            elif not ntuser.is_reviewed:
                context['error'] = "under_review"
                return render(request, 'dashboard/list-posts.html',context)

            elif not is_action_permitted('view_post_self', ntuser, site):
                context['error'] = "not_permitted"
                return render(request, 'dashboard/list-posts.html',context)
            else :
                posts = []
                page = int(request.GET.get('page',0) or request.POST.get('page', 0))
                # print(type(page))
                SORT_DEF = 'update'
                STATUS_DEF = 'all'
                ORDER_DEF = 'desc'
                ACTION_DEF = None
                RECORDS_DEF = 5
                action = request.GET.get('action',ACTION_DEF) or request.POST.get('action', ACTION_DEF)
                news_list = request.GET.getlist('posts',ACTION_DEF) or request.POST.getlist('posts', ACTION_DEF)
                # print(action)
                all_actions = ['publish', 'submit', 'reject', 'junk' ]

                if action in all_actions  and news_list is not None and len(news_list)>0:
                    news_list = list(map(int, news_list))
                    output = perform_news_list_action(action , news_list, context['user_can'],  ntuser)
                    if output is not True:
                        context['action_error'] = True
                        context['action_error_data'] = output
                        context['action_string'] = action
                    # print(news_list)

                sortby = request.GET.get('sort',SORT_DEF) or request.POST.get('sort', SORT_DEF)
                status = request.GET.get('status',STATUS_DEF) or request.POST.get('status', STATUS_DEF)
                order = request.GET.get('order',ORDER_DEF) or request.POST.get('order', ORDER_DEF)
                search = request.GET.get('search',"") or request.POST.get('search', "")
                tag = int(request.GET.get('tag',0) or request.POST.get('tag', 0))
                author = int(request.GET.get('author',0) or request.POST.get('author', 0))
                category = int(request.GET.get('category',0) or request.POST.get('category', 0))
                records = int(request.GET.get('records',0) or request.POST.get('records', 0))
                # print(records)
                sort_val = {
                    'publish':'published',
                    'title':'title',
                    'update':'updated'
                }
                status_val = {
                    'all':'all',
                    'draft':'draft',
                    'published' :'published',
                    'review':'under_review',
                    'rejected': 'rejected',
                    'trash':'trash',
                }
                order_val ={
                    'asc':"",
                    'desc':"-"
                }
                if sortby not in list(sort_val.keys()):
                    sortby = SORT_DEF
                # print(sites)
                if status not in list(status_val.keys()):
                    status = STATUS_DEF
                if order not in list(order_val.keys()):
                    order = ORDER_DEF
                query_str = "?status="+status
                if records == 0:
                    records = RECORDS_DEF
                if page == 0:
                    page=1
                # if not search =="":
                #     query_str += "&search="+search
                # if not category =="":
                #     query_str +="&category="+category
                # if not category =="":
                #     query_str +="&category="+category

                if is_action_permitted('view_post_other',ntuser, site):
                    if not author == 0:
                        posts = News.objects.filter(category__site__in = [site], author=author)
                    else:
                        posts = News.objects.filter(category__site__in = [site])
                else:
                    # print(sites)
                    posts = News.objects.filter(category__site__in = [site], author=user)

                context['all_count'] = posts.count()
                context['draft_count'] = posts.filter(status=status_val['draft']).count()
                context['review_count'] = posts.filter(status=status_val['review']).count()
                context['publish_count'] = posts.filter(status=status_val['published']).count()
                context['reject_count'] = posts.filter(status=status_val['rejected']).count()
                context['trash_count'] = posts.filter(status=status_val['trash']).count()

                if not category  == 0:
                    posts = posts.filter(category__id = category)

                if not tag  == 0:
                    posts = posts.filter(tag__id = tag)

                # print(status_val[status])
                if not status_val[status] == 'all':
                    posts = posts.filter(status=status_val[status])

                if not search == "":
                    posts = posts.filter(title__contains = search)

                if not author == "":
                    posts = posts.filter(title__contains = search)

                posts = posts.order_by(order_val[order]+sort_val[sortby])

                context['status'] = status
                context['search'] = search
                context['order'] = order
                context['sort'] = sortby
                context['count'] = posts.count()
                context['records'] = records
                context['record_list'] = [5,10,20,30]
                paginator = Paginator(posts, records)
                # print(paginator.page_range)
                total = len(list(paginator.page_range))
                if context['count']>0 and page in paginator.page_range:
                    pobj = paginator.page(page)
                    context['page'] = page
                    context['first_page'] = 1
                    context['last_page'] = total
                    context['prev_page'] = page-1
                    context['next_page'] = page+1

                    # print(page)
                    context['news'] = pobj.object_list
                    context['range'] = paginator.page_range
                    context['paginator'] = pobj
                    # context['paginator'] = paginator
                    context['edit_link'] = '/account/post/edit/'
                    context['review_link'] = '/account/post/review/'
                    context['assign_link'] = '/account/post/assign/'


                    # print(context)
                    return render(request, 'dashboard/list-posts.html',context)
                else:
                    context['error'] = "nothing_fetched"
                    return render(request, 'dashboard/list-posts.html',context)




        except NTUser.DoesNotExist:
            context = {}
            context['error'] = "not_applicable"
            return render(request, 'dashboard/add-post.html',context)

    return render(request, 'usermgt/user-account.html')


@login_required(login_url='{}{}'.format(settings.SITE_URL , settings.LOGIN_URL))
def assign_posts(request, slug):
    if request.user.is_authenticated:
        user = User.objects.get(id=request.user.id)
        try:
            ntuser = NTUser.objects.get(user=user)
            news = News.objects.filter(slug__slug = slug).first()
            if not news is None:
                site = news.get_news_site()
            else:
                context = {}
                context['error'] = "not_applicable"
                return render(request, 'dashboard/assign-posts.html',context)

            submitted = False
            # site = request.GET.get('site') or request.POST.get('site')
            # if site not in DEFINED_SITES:
            #     site = 'en'
            context = get_ntuser_context(ntuser, site)
            context['action']="Assign"

            # print(ntuser.user_type)
            # print(ntuser.user.id)
            # print(slug)
            # sites = get_NTUser_sites(ntuser)
            if ntuser.is_rejected:
                context['error'] = "rejected"
                context['message'] = ntuser.rejection_msg
                return render(request, 'dashboard/assign-posts.html',context)

            elif not ntuser.is_reviewed and ntuser:
                context['error'] = "under_review"
                return render(request, 'dashboard/assign-posts.html',context)

            elif not is_action_permitted('assign_review', ntuser, site):
                context['error'] = "not_permitted"
                return render(request, 'dashboard/assign-posts.html',context)
            else :


                if news is not None:
                    obj  = Activity.objects.filter(ntuser__user = user,activity_type=3, item_id = news.id,item_type = 'post').first()
                    if obj is not None:
                        form  = AssignPostsForm(instance=obj, data=request.POST or None)
                    else:
                        form  = AssignPostsForm(initial={ 'activity_type': 3, 'item_type': 'post', 'item_id':news.id }, data=request.POST or None)

                    if form.is_valid():
                         activity = form.save()
                         activity.comment= "Assigned for review"
                         activity.save()

                    context['form'] = form
                    context['news'] = news


                # print(action)
                return render(request, 'dashboard/assign-posts.html',context)



        except NTUser.DoesNotExist:
            context = {}
            context['error'] = "not_applicable"
            return render(request, 'dashboard/assign-posts.html',context)

    return render(request, 'usermgt/user-account.html')


@login_required(login_url='{}{}'.format(settings.SITE_URL , settings.LOGIN_URL))
def review_posts(request, slug):
    if request.user.is_authenticated:
        user = User.objects.get(id=request.user.id)
        try:
            ntuser = NTUser.objects.get(user=user)
            submitted = False
            news = News.objects.filter(slug__slug = slug).first()
            if not news is None:
                site = news.get_news_site()
            else:
                context = {}
                context['error'] = "not_applicable"
                return render(request, 'dashboard/assign-posts.html',context)
            # site = request.GET.get('site') or request.POST.get('site')
            # if site not in DEFINED_SITES:
            #     site = 'en'
            context = get_ntuser_context(ntuser, site)
            context['action']="Review"

            # print(ntuser.user_type)
            # print(ntuser.user.id)
            # print(slug)
            # print(request.POST.get('finalise'))
            # sites = get_NTUser_sites(ntuser)
            submitted = False
            if ntuser.is_rejected:
                context['error'] = "rejected"
                context['message'] = ntuser.rejection_msg
                return render(request, 'dashboard/assign-posts.html',context)

            elif not ntuser.is_reviewed and ntuser:
                context['error'] = "under_review"
                return render(request, 'dashboard/assign-posts.html',context)

            elif not is_action_permitted('review_other', ntuser, site):
                context['error'] = "not_permitted"
                return render(request, 'dashboard/assign-posts.html',context)
            else :
                # 'title','content', 'author', 'image', 'imagesource','sourcedesc', 'post'
                # news = News.objects.filter(slug__slug = slug).first()
                if news is not None:
                    postform = PostHistoryForm(initial={
                        'title':news.title,
                        'content': news.content,
                        'author': ntuser.user, # news.author,
                        # 'image':news.image,
                        # 'imagesource':news.imagesource,
                        # 'sourcedesc':news.sourcedesc,
                        'post':news
                        },
                        data=request.POST or None,
                        files=request.FILES or None)
                    prev_reviews  = Activity.objects.filter(item_id = news.id, item_type = 'post').order_by('date')
                    context['prev_reviews'] = prev_reviews
                    form  = ReviewPostsForm(initial={ "ntuser" : ntuser,'activity_type': 4, 'item_type': 'post', 'item_id':news.id }, data=request.POST or None)
                    # print(request.POST.get("image"))
                    # print(postform.get)
                    # if request.FILES is None:
                        # print("file None")
                    # if request.FILES:
                        # print(request.FILES['image'])
                    # if review.image == '' and :

                    if check_user_assigned_for_review(user, news):
                        if form.is_valid() and postform.is_valid():
                            history = postform.save()
                            review = form.save()
                            review.history_id = history.id

                            if user not in news.contributers.all():
                                 news.contributers.add(user)

                            if request.POST.get('finalise'):
                                news.status = 'final'
                                review.activity_type = 5
                                news.title = history.title
                                news.content = history.content
                            elif news.status == 'under_review':
                                news.status = 'awaiting_changes'

                            review.save()
                            news.save()

                        context['form'] = form
                        context['postform'] = postform
                        context['news'] = news

                # print(action)
                return render(request, 'dashboard/review-post.html',context)



        except NTUser.DoesNotExist:
            context = {}
            context['error'] = "not_applicable"
            return render(request, 'dashboard/assign-posts.html',context)

    return render(request, 'usermgt/user-account.html')



@login_required(login_url='{}{}'.format(settings.SITE_URL , settings.LOGIN_URL))
def preview_post_changes(request, hist_id):
    if request.user.is_authenticated:
        user = User.objects.get(id=request.user.id)
        try:
            ntuser = NTUser.objects.get(user=user)
            submitted = False
            hist = NewsHistory.objects.filter(id = hist_id).first()
            if hist is not None:
                site = hist.post.get_news_site()
            else:
                context = {}
                context['error'] = "not_applicable"
                return render(request, 'dashboard/assign-posts.html',context)
            # site = request.GET.get('site') or request.POST.get('site')
            #
            # if site not in DEFINED_SITES:
            #     site = 'en'
            context = get_ntuser_context(ntuser, site)
            context['action']="Preview"

            # print(ntuser.user_type)
            # print(ntuser.user.id)
            # print(slug)
            # print(request.POST.get('finalise'))
            # sites = get_NTUser_sites(ntuser)
            submitted = False
            if ntuser.is_rejected:
                context['error'] = "rejected"
                context['message'] = ntuser.rejection_msg
                return render(request, 'dashboard/assign-posts.html',context)

            elif not ntuser.is_reviewed and ntuser:
                context['error'] = "under_review"
                return render(request, 'dashboard/assign-posts.html',context)

            elif not is_action_permitted('preview_post_other', ntuser, site):
                context['error'] = "not_permitted"
                return render(request, 'dashboard/assign-posts.html',context)
            else :
                hist = NewsHistory.objects.filter(id = hist_id).first()
                if hist is not None:
                    context['news'] = hist.post

                    context['review'] = hist
                    context['edit_link'] = '/account/post/edit'
                    context['site']  = SiteSettings.objects.get(site=hist.post.get_news_site())
                print(hist)
                # print(action)
                return render(request, 'dashboard/news-preview.html',context)



        except NTUser.DoesNotExist:
            context = {}
            context['error'] = "not_applicable"
            return render(request, 'dashboard/assign-posts.html',context)

    return render(request, 'usermgt/user-account.html')

def incorporate_history(history_id):
    hist = NewsHistory.objects.filter(id = history_id).first()
    if hist is not None:
        pass
    else:
        return False
    pass
