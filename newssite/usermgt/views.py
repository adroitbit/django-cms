from django.shortcuts import render,redirect
from .forms import UserForm, LoginForm, UserApply, AddPostForm
from .models import NTUser
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import redirect

from django.urls import reverse
from django.contrib.auth.decorators import login_required
# from django.utils import timezone
# from datetime import datetime
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.utils.html import strip_tags
from datetime import datetime

from .functions import get_ntuser_context , get_postlist_sites
from .functions import get_slug_news , is_action_permitted, check_lang_error, get_NTUser_sites, get_user_can
from .models import RoleObj
from .functions import get_ntuser_context

from django.core import serializers
import json

from django.http import JsonResponse

from news.models import News, NewsHistory
from usermgt.models import Activity
from django.conf import settings
from news.models import DEFINED_SITES

#     (1,"Editor"),
#     (2,"Reviewer"),
#     (3, "Chief Editor"),
#     (4, "Content Manager"),

@login_required(login_url='{}{}'.format(settings.SITE_URL , settings.LOGIN_URL))
def account(request, site="en"):
    if request.user.is_authenticated:
        user = User.objects.get(id=request.user.id)
        try:
            ntuser = NTUser.objects.get(user=user)
            submitted = False
            # context = get_ntuser_context(ntuser)
            # site = request.GET.get('site') or request.POST.get('site')
            # if site not in DEFINED_SITES:
            #     site = 'en'
            allsite = get_postlist_sites(ntuser)
            print(allsite)
            if len(allsite) >0 and site not in allsite:
                return redirect(settings.DASHBOARD_URLS.account + allsite[0])

            context = get_ntuser_context(ntuser, site)
            print(context)
            return render(request, 'dashboard/dashboard.html',context)

        except NTUser.DoesNotExist:
            context = {
                'error': "not_applicable",
                'dashboard_links' : settings.DASHBOARD_URLS,
            }
            return render(request, 'dashboard/add-post.html',context)



@login_required(login_url='{}{}'.format(settings.SITE_URL , settings.LOGIN_URL))
def publish_post(request, postid):
    response= {}
    response['success']= False
    response['message']= ""
    if request.user.is_authenticated:
        user = User.objects.get(id=request.user.id)
        try:
            ntuser = NTUser.objects.get(user=user)
            submitted = False
            if not ntuser.is_reviewed:
                response['message'] = "User {}'s application for role {} is under review.".format(ntuser.user.get_full_name(), RoleObj[ntuser.user_type])

            elif ntuser.is_rejected:
                response['message']  = "User {}'s application for role {} is rejected.".format(ntuser.user.get_full_name(), RoleObj[ntuser.user_type])

            else:
                sites = get_NTUser_sites(ntuser)
                newsInstance = News.objects.get(id=postid)
                if newsInstance is not None:

                    if  not is_action_permitted('publish_self',ntuser, site) and newsInstance.author.id is ntuser.user.id:
                        response['message'] = "User {} is not permitted to publish posts.".format(ntuser.user.get_full_name())

                    elif not is_action_permitted('publish_other',ntuser, site) and newsInstance.author.id is not ntuser.user.id:
                        response['message'] = "User {} is not permitted to publish other's posts.".format(ntuser.user.get_full_name())

                    elif (not is_action_permitted('publish_other_site', ntuser, site)) and (newsInstance.get_news_site() not in sites):
                        response['message'] = "User {} is not permitted to publish other sites.".format(ntuser.user.get_full_name())

                    else:
                        # main publish logic
                        newsInstance.status = 'published'
                        newsInstance.save()
                        response['message'] = "Successfuly Published News!"
                        response['success'] = True

        except NTUser.DoesNotExist:
            response['message'] = "User {} is not allowed to publish posts.".format(ntuser.user.get_full_name())
        except News.DoesNotExist:
            response['message'] = "No post found with provided data."
    return JsonResponse(response)

def add_history_record(news, ntuser):

    hist = NewsHistory.objects.create(
        post=news,
        title = news.title,
        content = news.content,
        author = news.author,
    )
    Activity.objects.create(
        ntuser=ntuser,
        activity_type = 0,
        item_type = 'post',
        item_id = news.id,
        history_id = hist.id,
        comment = "Added post",
        )

def edit_history_record(news, ntuser):

    hist = NewsHistory.objects.create(
        post=news,
        title = news.title,
        content = news.content,
        author = news.author,
    )
    # print(hist)
    Activity.objects.create(
        ntuser=ntuser,
        activity_type = 1,
        item_type = 'post',
        item_id = news.id,
        history_id = hist.id,
        comment = "Edited post",
        )

@login_required(login_url='{}{}'.format(settings.SITE_URL , settings.LOGIN_URL))
def add_post(request, site):
    if request.user.is_authenticated:
        user = User.objects.get(id=request.user.id)
        # print(user)
        try:
            ntuser = NTUser.objects.get(user=user)
            # print(ntuser)
            submitted = False
            # site = request.GET.get('site') or request.POST.get('site')
            # if site not in DEFINED_SITES:
            #     site = 'en'
            context = get_ntuser_context(ntuser, site)
            context['action']= "Add"

            if not is_action_permitted('add_post',ntuser, site):
                context['error'] = "not_permitted"
                return render(request, 'dashboard/add-post.html',context)
            elif ntuser.is_rejected:
                context['error'] = "rejected"
                context['message'] = ntuser.rejection_msg
                return render(request, 'dashboard/add-post.html',context)

            elif not ntuser.is_reviewed:
                context['error'] = "under_review"
                return render(request, 'dashboard/add-post.html',context)

            else :
                # print(type(ntuser.allowed_sites))
                # print(len(ntuser.allowed_sites))
                # print(dir(ntuser.allowed_sites))
                # print(ntuser.allowed_sites.choices)
                # print(ntuser.allowed_sites.__getitem__(2))
                # print(ntuser.allowed_sites.__len__())
                sites = get_NTUser_sites(ntuser)
                # print(sites)
                addpost_form = AddPostForm(initial={'author': User.objects.get(id=request.user.id)}, data=request.POST or None, files=request.FILES or None, sites=sites)
                # print(addpost_form)

                if request.method == 'POST':
                    # print(addpost_form.errors)
                    if addpost_form.is_valid():
                        # print(addpost_form.cleaned_data['category'])
                        # if check_lang_error(sites,addpost_form.category ):
                        #     pass

                        post = addpost_form.save()
                        add_history_record(post, ntuser)
                        print("post saved")
                        context['postslug'] = post.slug.slug
                        submitted = True
                        addpost_form = AddPostForm(initial={'author': User.objects.get(id=request.user.id)}, sites=sites)
                context['form'] = addpost_form
                context['submitted'] = submitted
                # print(context)
                return render(request, 'dashboard/add-post.html',context)

        except NTUser.DoesNotExist:
            context['error'] = "not_applicable"
            return render(request, 'dashboard/add-post.html',context)

@login_required(login_url='{}{}'.format(settings.SITE_URL , settings.LOGIN_URL))
def edit_post(request, slug):
    if request.user.is_authenticated:
        user = User.objects.get(id=request.user.id)
        try:
            ntuser = NTUser.objects.get(user=user)
            submitted = False
            is_publish = True if request.GET.get('publish', False) == '1' else False
            newsInstance = get_slug_news(slug=slug)
            if newsInstance is not None:
                site = newsInstance.get_news_site()
            else:
                context = {}
                context['error'] = "404"
                return render(request, 'dashboard/add-post.html',context)
            # print(is_publish)
            # context = {
            #     'user':ntuser.user.get_full_name(),
            #     'role':RoleObj[ntuser.user_type],
            #     'message':"",
            #     'form':"",
            #     'submitted': False,
            #     'postslug': "",
            #     'action':"Edit" if not is_publish else "Publish",
            #     'user_can': get_user_can(ntuser.user_type),
            #     'error': '',
            #     'publish' : is_publish,
            #     'dashboard_links' : settings.DASHBOARD_URLS,
            #     }
            context = get_ntuser_context(ntuser, site)
            context['action']= "Edit" if not is_publish else "Publish",
            # print(ntuser.user_type)
            # print(ntuser.user.id)
            # print(slug)
            sites = get_NTUser_sites(ntuser)
            # print(sites)
            # sites = [ntuser.allowed_sites.__getitem__(i) for i in range(0,ntuser.allowed_sites.__len__())]

            # print(newsInstance is not None)
            if ntuser.is_rejected:
                context['error'] = "rejected"
                context['message'] = ntuser.rejection_msg
                return render(request, 'dashboard/add-post.html',context)

            elif not ntuser.is_reviewed:
                context['error'] = "under_review"
                return render(request, 'dashboard/add-post.html',context)

            else :
                # print(type(ntuser.allowed_sites))
                # print(len(ntuser.allowed_sites))
                # print(dir(ntuser.allowed_sites))
                # print(ntuser.allowed_sites.choices)
                # print(ntuser.allowed_sites.__getitem__(2))
                # print(ntuser.allowed_sites.__len__())
                if newsInstance is not None:
                    # print(is_action_permitted('edit_post_self',ntuser.user_type))
                    # print(is_action_permitted('edit_post_other',ntuser.user_type))
                    # print(ntuser.user_type)
                    # print(newsInstance.get_news_site())
                    if not is_action_permitted('edit_post_self',ntuser, site) and newsInstance.author.id is ntuser.user.id:
                        context['error'] = "not_permitted"
                        return render(request, 'dashboard/add-post.html',context)

                    elif not is_action_permitted('edit_post_other',ntuser, site) and newsInstance.author.id is not ntuser.user.id:
                        context['error'] = "not_permitted_other"
                        return render(request, 'dashboard/add-post.html',context)
                    elif newsInstance.get_news_site() not in sites and  not is_action_permitted('edit_post_other_lang',ntuser, site):
                        context['error'] = "not_permitted_other_lang"
                        return render(request, 'dashboard/add-post.html',context)
                    else:
                        addpost_form = AddPostForm(initial={'author': newsInstance.author},
                                                    data=request.POST or None, files=request.FILES or None, sites=sites,
                                                    instance=newsInstance)
                        # print(addpost_form)
                        if request.method == 'POST':
                            # print(addpost_form.errors)
                            if addpost_form.is_valid():
                                post = addpost_form.save()
                                # print("post modified")
                                submitted = True
                                context['postslug'] = post.slug.slug
                                if request.POST.get('status',"Save Draft") ==  "Save Draft":
                                    post.status = "draft"

                                elif request.POST.get('status',"Save Draft") ==  "Submit review":
                                    post.status = "under_review"

                                elif request.POST.get('status',"Save Draft") ==  "Publish" and context['user_can']['publish_other']:
                                    post.status = "published"
                                    post.published = datetime.now()

                                elif request.POST.get('status',"Save Draft") ==  "Re-Review":
                                    post.status = "under_review"

                                if not user.username == post.author.username and user not in post.contributers.all():
                                    post.contributers.add(user)

                                print(context['user_can']['publish_other'])
                                post.save()
                                edit_history_record(post, ntuser)
                        context['form'] = addpost_form
                        context['submitted'] = submitted
                        return render(request, 'dashboard/add-post.html',context)
                else:
                    context = {}
                    context['error'] = "404"
                    return render(request, 'dashboard/add-post.html',context)

        except NTUser.DoesNotExist:
            context = {}
            context['error'] = "not_applicable"
            return render(request, 'dashboard/add-post.html',context)

        except News.DoesNotExist:
            context = {}
            context['error'] = "404"
            return render(request, 'dashboard/add-post.html',context)

@login_required(login_url='{}{}'.format(settings.SITE_URL , settings.LOGIN_URL))
def user_logout(request):
    if request.user.is_authenticated:
        logout(request)
        return HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)
    # return render(request, 'usermgt/user-account.html')


def user_login(request):
    # print(request.user.is_authenticated)
    if request.user.is_authenticated:
        return HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)
    else:
        form = LoginForm(request.POST)
        if request.method == 'POST':
            if form.is_valid():
                username = form.cleaned_data['username']
                password = form.cleaned_data['password']
                user = authenticate(username=username, password=password)
                if user:
                    login(request,user)
                    return HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)

            else:
                print("failed login username: {} and password: {}".format(username,password))

        else:
            form = LoginForm()

        # return render(request, 'usermgt/user-login.html',
        return render(request, 'dashboard/user-login.html',
                        {
                        'form': form
                        })


@login_required(login_url='{}{}'.format(settings.SITE_URL , settings.LOGIN_URL))
def apply_as(request,role):
    # print(role)
    applied = False
    roles = ['editor', 'reviewer', 'chiefeditor', 'advertiser', 'seoexpert']
    user_roles = {
        'editor':1,
        'reviewer': 2,
        'chiefeditor': 3,
        'advertiser': 7,
        'seoexpert': 6,
    }
    if request.user.is_authenticated:
        user = User.objects.get(id=request.user.id)
        applied = False
        try:
            ntuser = NTUser.objects.get(user=user)
            application_form = UserApply(data=request.POST or None, files=request.FILES or None, instance=ntuser)

            if ntuser is not None:

                if request.method == 'POST':
                    # application_form = UserApply(data=request.POST, files=request.FILES, instance=ntuser)
                    # print(application_form.is_valid())
                    # print(application_form.errors)
                    # print(request.FILES.get('proof1'))
                    # print(request.FILES.get('profile_pic'))
                    # print(application_form.cleaned_data['user_type'])
                    if role in roles and application_form.is_valid():
                        # print(ntuser.proof1)
                        # print(ntuser.country_code)
                        # print(ntuser.profile_pic)
                        # ntuser.destroyRecord()
                        # user
                        application_form.save()
                        # profile = form.save(commit=False)
                        # ntuser.user_type = int(application_form.cleaned_data["user_type"])
                        # ntuser.country_code = application_form.cleaned_data["country_code"]
                        # # ntuser.user_type = application_form.cleaned_data["user_type"]
                        # ntuser.proof1 = upload_file(request, 'proof1')
                        # ntuser.profile_pic = upload_file(request, 'profile_pic')
                        # # print(ntuser.proof1)
                        # # print(ntuser.profile_pic)

                        # if 'proof2' in request.FILES:
                        #     ntuser.proof2 =  upload_file(request, 'proof2')
                        # if 'bio' in application_form.cleaned_data:
                        #     ntuser.bio = strip_tags(application_form.cleaned_data["bio"])
                        # if 'twitter' in application_form.cleaned_data:
                        #     ntuser.twitter = application_form.cleaned_data["twitter"]
                        # if 'blog' in application_form.cleaned_data:
                        #     ntuser.blog = application_form.cleaned_data["blog"]
                        applied = True
                else:
                    #user opened the apply as link
                    if role in roles:
                        #if the role is what we expect
                        application_form = UserApply(initial={'user_type': user_roles[role]}, instance=ntuser)

                    else:
                        return render(request, 'usermgt/user-404.html')

                return render(request, 'usermgt/user-apply.html',{
                            'role':role,
                            'applied': applied,
                            'userform':application_form,
                            })
        except NTUser.DoesNotExist:
            return render(request, 'usermgt/user-apply-not-applicable.html')




def register(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)

    else:
        registered = False
        if request.method == 'POST':
            user_form = UserForm(data=request.POST)
            # print(user_form)
            if user_form.is_valid():
                user = user_form.save()
                user.set_password(user.password)
                user.save()
                nt = NTUser()
                nt.user = user
                nt.save()
                # assigning subscriber role by default
                nt.assign_subscriber_role()
                # user = authenticate(username=user.username, password=user.password)
                # login(request,user)
                registered = True
            else:
                print(user_form.errors)
        else:
            user_form = UserForm()
            # profile_form = UserProfileInfoForm()
        # return render(request,'usermgt/user-register.html',
        return render(request, 'dashboard/create-account.html',
                              {
                              'userform':user_form,
                              'registered':registered,
                              })
