from django.db import models
from django.contrib.auth.models import User

# from filer.fields.image import FilerImageField

from news.models import DEFINED_SITES
# from multiselectfield import MultiSelectField
# from .roles import UserRoles , UserCapability


# Create your models here.
RoleObj = {
  0:"Subscriber",
  1:"Editor",
  2:"Reviewer",
  3:"Chief Editor",
  4:"Content Manager",
  5:"Site Admin",
  6:"SEO Expert",
  7:"Ad Publisher",
}
Roles =(
    (0, "Subscriber"),
    (1,"Editor"),
    (2,"Reviewer"),
    (3, "Chief Editor"),
    (4, "Content Manager"),
    (5, "Site Admin"),
    (6, "SEO Expert"),
    (7, "Ad Publisher"),
    )
Activity_choice = (
    (0, "Added"),
    (1, "Edited"),
    (2, "Marked ready for review"),
    (3, "Assigned for review"),
    (4, "Reviewed"),
    (5, "Finalised"),
    (6, "Published"),

    )

Activity_choice_DICT = {
    0: "Added",
    1: "Edited",
    2: "Marked ready for review",
    3: "Assigned for review",
    4: "Reviewed",
    5: "Finalised",
    6: "Published",
    }
Item_choice_DICT = {
    'post': "Post",
    'page': "Page",
    'ad': "Ad",
    }

Item_choice = (
    ('post', "Post"),
    ('page', "Page"),
    ('ad', "Ad"),
    )

class UserRoles(models.Model):
  class Meta:
        verbose_name_plural = "User Roles"
        verbose_name = "User Role"
  role = models.CharField(max_length=50, verbose_name = "User Role", unique=True)
  description = models.CharField(max_length=150,blank=True, null=True, verbose_name = "Role description")
  def __str__(self):
      if self.description:
          return self.role +" ("+self.description+")"
      else:
          return self.role

class UserCapability(models.Model):
    class Meta:
          verbose_name_plural = "User Capabilities"
          verbose_name = "User Capability"
    cap = models.CharField(max_length=50, verbose_name = "User Capability", unique=True)
    description = models.CharField(max_length=150, blank=True, null=True,verbose_name = "Capability description")
    def __str__(self):
        if self.description:
            return self.cap +" ("+self.description+")"
        else:
            return self.cap

class RoleCapability(models.Model):
    class Meta:
          verbose_name_plural = "Role Capabilities"
          verbose_name = "Role Capability"
    role = models.OneToOneField(UserRoles, on_delete=models.CASCADE, related_name ='user_role', verbose_name = "Role", unique=True)
    cap = models.ManyToManyField(UserCapability, related_name ='user_role_cap', verbose_name = "Allowed Capability")
    def __str__(self):
        return self.role.role

class NTUser(models.Model):
  class Meta:
        verbose_name_plural = "Users"
        verbose_name = "User"
  karma = models.PositiveIntegerField(default=0)
  bio = models.TextField(max_length=500, blank=True, default="")
  user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True, related_name ='user_profile', verbose_name = "User")
  is_reviewed = models.BooleanField(verbose_name = "Profile Reviewed", default=False)
  is_rejected = models.BooleanField(verbose_name = "Profile Rejected", default=False)
  email_varified = models.BooleanField(verbose_name = "Varified Email", default=False)
  accepted = models.BooleanField(verbose_name = "Accepted T&C", default=False)
  country_code = models.CharField(max_length=5, default='np')
  phone = models.CharField(max_length=20, null=True, blank=True, default = "", verbose_name = "Phone number")
  # proof1 = FilerImageField(on_delete=models.CASCADE,null=True, blank=True, default = None,related_name="proof_attachment1", verbose_name = "Citizenship Proof")
  proof1 = models.ImageField(upload_to='aaplications/%Y/%m/%d/',blank=True,null=True,  default = None, verbose_name = "Citizenship Proof")
  # proof2 = FilerImageField(on_delete=models.CASCADE,null=True, blank=True, default = None,related_name="proof_attachment2", verbose_name = "Other Proof")
  proof2 = models.ImageField(upload_to='aaplications/%Y/%m/%d/', blank=True, null=True,  default = None, verbose_name = "Other Proof")
  blog = models.URLField(max_length=200,null=True, blank=True, default = None, verbose_name = "Blog URL")
  twitter = models.CharField(max_length=50,null=True, blank=True, default = None, verbose_name = "Twitter Handle")
  profile_pic = models.ImageField(upload_to='profile/%Y/%m/%d/',blank=True,null=True,  default = None, verbose_name = "User's Image")
  # profile_pic = FilerImageField(on_delete=models.CASCADE,null=True, blank=True, default = None,related_name="profile_attachment", verbose_name = "User's Image")
  # allowed_sites = MultiSelectField(choices=DEFINED_SITES, default = 'en', max_choices=2)
  primary_site = models.CharField(max_length=4, choices =DEFINED_SITES, default = 'en', verbose_name = "Primary site")
  user_type = models.ForeignKey(UserRoles, on_delete=models.CASCADE, default = None,blank=True, null=True,related_name ='nt_user_role', verbose_name = "Primary site role")
  secondary_site = models.CharField(max_length=4, choices =DEFINED_SITES, default = None,blank=True, null=True, verbose_name = "Secondary site")
  secondary_user_type = models.ForeignKey(UserRoles, on_delete=models.CASCADE, default = None,blank=True, null=True,related_name ='nt_user_second_role', verbose_name = "Secondary site role")
  rejection_msg = models.CharField(max_length=200, null=True, blank=True, default = "", verbose_name = "Rejection Message")

  def assign_user_role(self, site, role, strict=False):
      roleobj = UserRoles.objects.filter(role=role).first()
      if not roleobj is None:
          print("assign_user_role {}".format(site))
          return self.add_site_capabilities(site, roleobj, strict)
      else:
          print("{} role not found".format(role))
          return  False

  def getAllowedCapabilities(self,site):
      cap = AllowedCapability.objects.filter(ntuser=self,site=site).first()
      if cap is None:
          return False
      else:
          return cap

  def add_site_capabilities(self, site, role, strict=False):
      print("add_site_capabilities executing {}".format(site))
      cap = RoleCapability.objects.filter(role=role).first()
      print(cap.cap.all())
      allowed_present = self.getAllowedCapabilities(site)
      if allowed_present is False:
          allowed = AllowedCapability()
          allowed.ntuser= self
          allowed.site = site
          allowed.save()
          allowed.cap.set(cap.cap.all())
          allowed.save()
          return True
      else:
          if strict:
              for c in allowed_present.cap.all():
                  allowed_present.cap.remove(c)

          for c in cap.cap.all():
              if allowed_present.cap.filter(cap=c).first() is None:
                  allowed_present.cap.add(c)
                  # allowed_present.cap.save()
          allowed_present.save()
          return True

  def assign_subscriber_role(self):
      for site in DEFINED_SITES:
          print("Assigning to site {}".format(site[0]))
          self.assign_user_role(site[0], "Subscriber")



  def save(self,*args, **kwargs):
      if self.primary_site and self.user_type:
          self.add_site_capabilities(self.primary_site,  self.user_type, strict=True)

      if self.secondary_site and self.secondary_user_type:
          self.add_site_capabilities(self.secondary_site,  self.secondary_user_type, strict=True)

      super(NTUser, self).save(*args, **kwargs)

  def __str__(self):
        return self.user.get_full_name() or self.user.username

class Activity(models.Model):
  class Meta:
        verbose_name_plural = "Activities"
        verbose_name = "Activity"

  ntuser = models.ForeignKey(NTUser, on_delete=models.CASCADE, related_name ='nt_user', verbose_name = "User")
  activity_type = models.PositiveIntegerField(choices=Activity_choice, default=0)
  item_type = models.CharField(max_length=10,choices=Item_choice, default="Item type")
  item_id = models.PositiveIntegerField(default=0)
  history_id = models.PositiveIntegerField(default=0)
  date = models.DateTimeField(auto_now=True, verbose_name = "Activity Date")
  comment = models.TextField(blank=True, null=True, verbose_name = "Comment")
  finalise = models.BooleanField(verbose_name = "Finalised", default=False)

  def __str__(self):
      return self.ntuser.user.get_full_name() + " " + Activity_choice_DICT[self.activity_type] + " " +Item_choice_DICT[self.item_type]


class AllowedCapability(models.Model):
    class Meta:
          verbose_name_plural = "Allowed Capabilities"
          verbose_name = "Allowed Capability"
          unique_together = [['ntuser', 'site']]

    ntuser = models.ForeignKey(NTUser, on_delete=models.CASCADE, related_name ='nt_cap_user', verbose_name = "User")
    cap = models.ManyToManyField(UserCapability, related_name ='nt_user_capabilities', verbose_name = "Allowed Capability")
    site = models.CharField(max_length=4, choices =DEFINED_SITES, default = 'en')
