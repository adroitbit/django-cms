from django.core.files.storage import FileSystemStorage
from news.models import News, SiteSlug

from usermgt.models import Activity, AllowedCapability
# from .functions import  assign_user_role
from django.conf import settings

from usermgt.models import RoleCapability, UserCapability

from theme.models import SiteSettings
from news.models import DEFINED_SITES


def get_site_slug(slug):
    return SiteSlug.objects.filter(slug=slug).first()
    # try:
    #     return SiteSlug.objects.get(slug=slug)
    # except SiteSlug.DoesNotExist:
    #     return None


def get_ntuser_context(ntuser, site):
    siteobj = SiteSettings.objects.filter(site__in =get_postlist_sites(ntuser))

    return  {
                'user': ntuser.user.get_full_name() or ntuser.user.username,
                'user_obj':ntuser.user,
                'role': ntuser.user_type,
                'message':"",
                'form':"",
                'submitted': False,
                'user_can': get_user_can(ntuser, site),
                'error': '',
                'urls' : settings.DASHBOARD_URLS,
                'allsite': siteobj,
                'site':site,
    }

def get_slug_news(slug):
    slugobj = get_site_slug(slug)
    if slugobj is not None:
            return  News.objects.filter(slug=slugobj).first()
    else:
        return None

# Roles =(
#     (0, "Subscriber"),
#     (1,"Editor"),
#     (2,"Reviewer"),
#     (3, "Chief Editor"),
#     (4, "Content Manager"),
#     (5, "Site Admin"),
#     (6, "SEO Expert"),
#     (7, "Ad Publisher"),
#     )

# def action_dict(action):
#     return {
#         'add_post'                  : {0:False,1:True,2:True,3:True,4:True,5:True,6:False,7:False},
#         'view_post_self'            : {0:False,1:True,2:True,3:True,4:True,5:True,6:False,7:False},
#         'edit_post_self'            : {0:False,1:True,2:True,3:True,4:True,5:True,6:False,7:False},
#         'view_post_other'           : {0:False,1:False,2:True,3:True,4:True,5:True,6:False,7:False},
#         'edit_post_other'           : {0:False,1:False,2:True,3:True,4:True,5:True,6:False,7:False},
#         'edit_post_other_lang'      : {0:False,1:False,2:False,3:False,4:False,5:True,6:False,7:False},
#         'handle_post_other_lang'    : {0:False,1:False,2:False,3:False,4:False,5:True,6:False,7:False},
#         'publish_self'              : {0:False,1:False,2:False,3:True,4:True,5:True,6:False,7:False},
#         'publish_other'             : {0:False,1:False,2:False,3:True,4:True,5:True,6:False,7:False},
#         'publish_other_site'        : {0:False,1:False,2:False,3:False,4:False,5:True,6:False,7:False},
#
#         'edit_publish_self'         : {0:False,1:False,2:False,3:False,4:False,5:True,6:False,7:False},
#         'edit_publish_other'        : {0:False,1:False,2:False,3:False,4:False,5:True,6:False,7:False},
#         'unlock_posts'              : {0:False,1:False,2:False,3:False,4:False,5:True,6:False,7:False},
#          'lock_posts'
#         'review_other'          : {0:False,1:False,2:True,3:True,4:True,5:True,6:False,7:False},
#         'assign_review'         : {0:False,1:False,2:False,3:True,4:True,5:True,6:False,7:False},
#         'reject_post_other'           : {0:False,1:False,2:False,3:True,4:True,5:True,6:False,7:False},
#         'trash_other'           : {0:False,1:False,2:False,3:True,4:True,5:True,6:False,7:False},
#         'preview_post'          : {0:False,1:True,2:True,3:True,4:True,5:True,6:True,7:False},
#         'preview_page'          : {0:False,1:False,2:False,3:False,4:True,5:True,6:False,7:False},
#         'add_page'              : {0:False,1:False,2:False,3:False,4:True,5:True,6:False,7:False},
#         'edit_page_self'        : {0:False,1:False,2:False,3:False,4:True,5:True,6:False,7:False},
#         'edit_page_other'       : {0:False,1:False,2:False,3:False,4:False,5:True,6:False,7:False},
#         'review_page_other'     : {0:False,1:False,2:False,3:False,4:False,5:True,6:False,7:False},
#         'assign_review_page'    : {0:False,1:False,2:False,3:False,4:False,5:True,6:False,7:False},
#         'publish_page_self'     : {0:False,1:False,2:False,3:False,4:True,5:True,6:False,7:False},
#         'publish_page_other'    : {0:False,1:False,2:False,3:False,4:False,5:True,6:False,7:False},
#     }[action]

def is_action_permitted(action, ntuser, site):
    allowed = getAllowedCapabilities(ntuser, site)
    try:
         return check_user_allowed(action,allowed)
    except:
        return False

def check_same_lang_taxanomy(taxanomy):
    lang = taxanomy[0].site

    for tax in taxanomy:
        print(tax.site)
        print(taxanomy[0].site)
        if not lang == tax.site:
            print(tax.site)
            return True

    return lang

def check_lang_error(languages,categories):
    print(languages)
    lang_or_error = check_same_lang_taxanomy(categories)
    if lang_or_error is True:
        return "Error1"
    elif lang_or_error not in languages:
        return 'Error2'
    else:
        return False

# def get_NTUser_sites(ntuser):
#     if is_action_permitted('handle_post_other_lang',ntuser.user_type):
#         return [x[0] for x in DEFINED_SITES]
#     else:
#         return [ntuser.allowed_sites.__getitem__(i) for i in range(0,ntuser.allowed_sites.__len__())]

def get_NTUser_sites(ntuser):
    if ntuser.primary_site:
        return [ntuser.primary_site , ntuser.secondary_site]
    else:
        return [ntuser.primary_site]

def get_postlist_sites(ntuser):
    capobj = getCapabilityObj("view_post_self")
    allrecord = AllowedCapability.objects.filter(ntuser=ntuser,cap__in=[capobj])
    return [r.site for r in allrecord ]



def check_user_allowed(cap , allowed):
    capobj = getCapabilityObj(cap)
    try:
        if capobj in allowed.cap.all():
            return True
        else:
            return False
    except:
        return False


def get_user_can(ntuser, site):
    allowed = getAllowedCapabilities(ntuser, site)
    return {
        'add_post'                : check_user_allowed('add_post',allowed) ,
        'view_post_self'          : check_user_allowed('view_post_self',allowed),
        'edit_post_self'          : check_user_allowed('edit_post_self',allowed),
        'view_post_other'         : check_user_allowed('view_post_other',allowed),
        'edit_post_other'         : check_user_allowed('edit_post_other',allowed),
        'edit_post_other_lang'    : check_user_allowed('edit_post_other_lang',allowed),
        'handle_post_other_lang'  : check_user_allowed('handle_post_other_lang',allowed),
        'publish_self'            : check_user_allowed('publish_self',allowed),
        'publish_other'           : check_user_allowed('publish_other',allowed),
        'publish_other_site'      : check_user_allowed('publish_other_site',allowed),
        'review_other'            : check_user_allowed('review_other',allowed),
        'assign_review'           : check_user_allowed('assign_review',allowed),
        'preview_post_self'       : check_user_allowed('preview_post_self',allowed),
        'preview_post_other'      : check_user_allowed('preview_post_self',allowed),
        'preview_page'            : check_user_allowed('preview_page',allowed),
        'add_page'                : check_user_allowed('add_page',allowed),
        'edit_page_self'          : check_user_allowed('edit_page_self',allowed),
        'edit_page_other'         : check_user_allowed('edit_page_other',allowed),
        'review_page_other'       : check_user_allowed('review_page_other',allowed),
        'assign_review_page'      : check_user_allowed('assign_review_page',allowed),
        'publish_page_self'       : check_user_allowed('publish_page_self',allowed),
        'publish_page_other'      : check_user_allowed('publish_page_other',allowed),
        'reject_post_other'       : check_user_allowed('reject_post_other',allowed),
        'trash_other'             : check_user_allowed('trash_other',allowed),
    }


def perform_action_news(action, post):
    if action == "publish" and post.status == 'final':
        post.status = "published"
    elif action == "submit":
        post.status = "under_review"
    elif action ==  'reject' and not post.status == 'final':
        post.status = "rejected"
    elif action ==  'junk':
        post.status = "trash"
    post.save()



def perform_news_list_action(action,news_list, user_can,  ntuser):
    # print(user_can)
    selected_posts =News.objects.filter(id__in = news_list).all()
    allowed =  True
    errors = []
    if action == 'publish':
        for post in selected_posts:
            # allowed = allowed and check_user_can_publish(user_can, post, ntuser)
            if not check_user_can_publish(user_can, post, ntuser):
                errors.append(post)
                allowed = False

    elif action == 'reject':
        for post in selected_posts:
            #  allowed = allowed and check_user_can_reject(user_can, post, ntuser)
             if not check_user_can_reject(user_can, post, ntuser):
                errors.append(post)
                allowed = False


    elif action == 'junk':
        for post in selected_posts:
            #  allowed = allowed and check_user_can_trash(user_can, post, ntuser)
             if not check_user_can_trash(user_can, post, ntuser):
                errors.append(post)
                allowed = False

    if not allowed:
        return errors
    else:
        for post in selected_posts:
            perform_action_news(action, post)

        return True


def getCapabilityObj(cap):
    return UserCapability.objects.filter(cap=cap).first()

def getAllowedCapabilities(ntuser, site):
    cap = AllowedCapability.objects.filter(ntuser=ntuser,site=site).first()
    if cap is None:
        return False
    else:
        return cap

def check_user_can(cap, ntuser, site):
    allowed = getAllowedCapabilities(ntuser, site)
    capobj = getCapabilityObj(cap)
    if capobj in allowed.cap.all():
        return True
    else:
        return False

# def assign_subscriber_role(ntuser):
#     for site in DEFINED_SITES:
#         assign_user_role(ntuser, site[0], "Subscriber")
#
# def assign_user_role(ntuser, site, role, strict=False):
#     roleobj = UserRoles.objects.filter(role=role).first()
#     if roleobj:
#         return add_user_capabilities(ntuser, site, roleobj, strict)
#     else:
#         return  False

def check_user_can_publish(user_can, post, ntuser):
    try:
        own_post = check_user_own(post, ntuser)
        if own_post and user_can["publish_self"]:
            return True
        elif not own_post and user_can["publish_other"]:
            return True

        else:
            return False

    except:
        print("except")
        return False

def check_user_can_trash(user_can, post, ntuser):
    try:
        # print(user_can["trash_other"])
        own_post = check_user_own(post, ntuser)
        # print(post.author.username)
        # print(ntuser.user.username)
        # print(ntuser.user.username == post.author.username)

        # if ntuser.user.username == post.author.username:
        #     own_post =  True
        #     print("own post")

        if own_post :
            return True

        elif not own_post and user_can["trash_other"]:
            return True
        else:
            return False
    except:
        print("except")
        return False

def check_user_assigned_for_review(user, post):
    obj  = Activity.objects.filter(ntuser__user = user,activity_type=3, item_id = post.id,item_type = 'post').first()
    # print(obj)
    if obj is not None:
        return True
    else:
        return False



def check_user_can_assign(user_can, post, ntuser):
    try:

        if user_can["assign_review"]:
            return True
        else:
            return False
    except:
        print("except")
        return False

def check_user_own(post, ntuser):
    if ntuser.user.username == post.author.username:
        return True
    else:
        return False

def check_user_can_reject(user_can, post, ntuser):
    try:
        if user_can["reject_post_other"]:
            return True
        else:
            return False
    except:
        print("except")
        return False
