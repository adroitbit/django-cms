from django import forms
from django.contrib.auth.forms import UserCreationForm

from .models import NTUser
from django.contrib.auth.models import User
from news.models import News, Category, Tag, NewsHistory

from usermgt.models import Activity

# from ckeditor.widgets import CKEditorWidget

# from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget


from .functions import check_lang_error, get_NTUser_sites

# https://github.com/summernote/django-summernote
class AddPostForm(forms.ModelForm):
    def __init__(self,sites,*args,**kwargs):
        super (AddPostForm,self ).__init__(*args,**kwargs)
        # print(sites)
        self.sites = sites
        self.fields['category'].queryset = Category.objects.filter(site__in=sites)
        self.fields['tag'].queryset = Tag.objects.filter(site__in=sites)
        self.fields['title'].required = True
        self.fields['category'].required = True
        self.fields['author'].required = True

    def clean(self):
        cleaned_data = super(AddPostForm, self).clean()
        category = cleaned_data.get("category")
        tag = cleaned_data.get("tag")
        user = cleaned_data.get("author")
        # print(type(category))
        # print(category is None)
        # print(tag)
        # print(cleaned_data.get("content"))
        # print(type(user))
        # print(cleaned_data.get('image'))
        ntuser = NTUser.objects.get(user=user)
        print(check_lang_error(get_NTUser_sites(ntuser), category))
        if category is None:
            cat_error= None
            raise forms.ValidationError('User Must Add category to a Post')
        else:
            cat_error =check_lang_error(get_NTUser_sites(ntuser), category)

        if cat_error is 'Error1':
            raise forms.ValidationError('User Can Add Post to category of one language')
        elif cat_error is 'Error2':
            raise forms.ValidationError('User Not Allowed to add Post to category of a language that is not assigned to the author')

        if len(tag) >0:
            tag_error =check_lang_error(get_NTUser_sites(ntuser), tag)
            if tag_error is 'Error1':
                raise forms.ValidationError('User Can Add Post tag of one language')
            elif tag_error is 'Error2':
                raise forms.ValidationError('User Not Allowed to add Post to tag of a language that is not assigned to the author')

        # if cleaned_data.get('image'):
        #     if cleaned_data.get('imagesource') is None:
        #         raise forms.ValidationError('Please provide image source')
        #     elif cleaned_data.get('sourcedesc') is None:
        #         raise forms.ValidationError('Please give image source short description')

            # data = request.FILES['image']
            # pass



    class Meta():
        content = forms.CharField(label='Content',widget=SummernoteInplaceWidget())
        title = forms.CharField(widget=forms.TextInput())
        model = News
        widgets = {
            'content': SummernoteInplaceWidget(),
            'author': forms.HiddenInput(),
            'category': forms.CheckboxSelectMultiple(),
            'tag': forms.CheckboxSelectMultiple(),
            'title': forms.TextInput(attrs={'is_hidden':False,'class':'form-control form-control-user', 'id':'postTitle', 'placeholder':"Enter Post title"})
        }
        fields =  ('title','category','content','tag','image', 'imagesource', 'sourcedesc', 'author')


class UserForm(forms.ModelForm):
    # fields = ('first_name','last_name','username','password','email')
    class Meta():
        password = forms.CharField(widget=forms.PasswordInput())
        model = User
        widgets = {
            'password': forms.PasswordInput(attrs={'class':"form-control form-control-user",'placeholder':"Password"}),
            'first_name': forms.TextInput(attrs={'class':"form-control form-control-user",'placeholder':"First name"}),
            'last_name': forms.TextInput(attrs={'class':"form-control form-control-user",'placeholder':"Last name"}),
            'username': forms.TextInput(attrs={'class':"form-control form-control-user",'placeholder':"User name"}),
            'email': forms.TextInput(attrs={'class':"form-control form-control-user",'placeholder':"Email"}),
        }
        fields =  ('first_name','last_name','username','password','email')


class UserApply(forms.ModelForm):
    # fields = ('user_type','country_code', 'proof1', 'proof2', 'blog', 'twitter', 'profile_pic', 'allowed_sites')
    def __init__(self, *args, **kwargs):
        super(UserApply, self).__init__(*args, **kwargs)
        # Making location required
        self.fields['user_type'].required = True
        self.fields['country_code'].required = True
        self.fields['proof1'].required = True
        self.fields['profile_pic'].required = True

    class Meta():
        model = NTUser
        fields = ('user_type','country_code', 'proof1', 'proof2', 'blog', 'twitter', 'profile_pic', 'bio')
        fields_required = ['user_type','country_code', 'proof1', 'profile_pic' ]
        user_type = forms.CharField(widget=forms.HiddenInput())
        # proof1 = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))
        # proof2 = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))
        # profile_pic = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))
        widgets = {
            'user_type': forms.HiddenInput(),
        }



class LoginForm(forms.Form):
    # class Meta():
    #     widgets = {
    #         'password': forms.PasswordInput(attrs={'class':"form-control form-control-user",'placeholder':"Password"}),
    #         'username': forms.TextInput(attrs={'class':"form-control form-control-user",'placeholder':"User name"}),
    #     }

    username = forms.CharField(label='Username', max_length=40, widget=forms.TextInput(attrs={'class':"form-control form-control-user",'placeholder':"User name"}))
    password = forms.CharField(label='Password', max_length=40, widget=forms.PasswordInput(attrs={'class':"form-control form-control-user",'placeholder':"Password"}))


class AssignPostsForm(forms.ModelForm):
    def __init__(self,*args,**kwargs):
        super (AssignPostsForm,self ).__init__(*args,**kwargs)
        # print(sites)
        # self.fields['ntuser'].queryset = Category.objects.filter(site__in=sites)
        self.fields['item_type'].required = True
        self.fields['activity_type'].required = True
        self.fields['item_id'].required = True
        self.fields['ntuser'].required = True

    class Meta():
        model = Activity
        fields = ('ntuser','activity_type', 'item_type', 'item_id')
        widgets = {
            'activity_type': forms.HiddenInput(),
            'item_type': forms.HiddenInput(),
            'item_id': forms.HiddenInput(),
          }

class ReviewPostsForm(forms.ModelForm):
    def __init__(self,*args,**kwargs):
        super (ReviewPostsForm,self ).__init__(*args,**kwargs)
        # print(sites)
        # self.fields['ntuser'].queryset = Category.objects.filter(site__in=sites)
        self.fields['item_type'].required = True
        self.fields['activity_type'].required = True
        self.fields['item_id'].required = True
        self.fields['ntuser'].required = True
        self.fields['comment'].required = True
        self.fields['finalise'].required = False

        # self.fields['content'].required = True



    class Meta():
        model = Activity
        fields = ('ntuser','activity_type', 'item_type', 'item_id', 'comment', 'finalise')
        comment = forms.CharField(widget=forms.TextInput())

        widgets = {
            'activity_type'     : forms.HiddenInput(),
            'ntuser'            : forms.HiddenInput(),
            'item_type'         : forms.HiddenInput(),
            'item_id'           : forms.HiddenInput(),
            'comment'           : forms.Textarea(attrs={"placeholder":"Enter your review here", "required": True, "class":"w-100", "rows":3, "cols":83}),
        }

class PostHistoryForm(forms.ModelForm):
    class Meta():
        model = NewsHistory
        fields = ('title','content', 'author', 'post')
        content = forms.CharField(label='Content',widget=SummernoteInplaceWidget())
        # comment = forms.CharField(widget=forms.TextInput())
        title = forms.CharField(widget=forms.TextInput())
        widgets = {
            'post'  : forms.HiddenInput(),
            'content': SummernoteInplaceWidget(),
            'author': forms.HiddenInput(),
            'title': forms.TextInput(attrs={'is_hidden':False,'class':'form-control form-control-user', 'id':'postTitle', 'placeholder':"Enter Post title"})
        }
