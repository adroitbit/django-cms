from django.contrib import admin

# Register your models here.
from .models import NTUser, AllowedCapability
from .models import UserRoles, UserCapability, RoleCapability

admin.site.register(NTUser)


admin.site.register(UserRoles)

admin.site.register(UserCapability)

admin.site.register(RoleCapability)

admin.site.register(AllowedCapability)
