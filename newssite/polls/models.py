from django.db import models

# Create your models here.
class PollAnswer(models.Model):
    
    answer = models.CharField(max_length=120)
    email = models.EmailField()
    
class Polls(models.Model):
    
    question = models.CharField(max_length=120)
    answer1 = models.TextField()
    answer2 = models.TextField()
    answer3 = models.TextField()
    answer4 = models.TextField()
    StartDate = models.DateField()
    EndDate = models.DateField()