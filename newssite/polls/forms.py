from django import forms

from .models import PollAnswer

class PollForm(forms.ModelForm):
    answer = forms.CharField(required=True, widget=forms.Textarea(
								attrs={
								"class": "new-class-name two",
								"id": "my-id-for-textarea",
								"rows": 20,
								'cols': 100
								}))
    class Meta:
        model = PollAnswer
        
        fields = [
            
            'answer',
            'email',
            ]